#include <Windows.h>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include <regex>
#include <cctype>

#include "rapidxml.hpp"

using namespace std;
using namespace rapidxml;
namespace fs = std::filesystem;


std::vector<std::string> _related_FileName_Info;
std::vector<std::string> _related_Value_Info;
std::vector<std::string> _related_FileSizeValue_Info;
std::vector<std::string> _related_PosStack_Info;
std::vector<std::string> _related_AniStack_Info;


std::vector<std::string> _related_Repeat_Info;
std::vector<std::string> _related_Anchor_Info;
std::vector<std::string> _related_Trans_Info;
std::vector<std::string> _related_Rot_Info;
std::vector<std::string> _related_Effect_Info;


std::vector<std::string> _related_Background_Info;
std::vector<std::string> _related_Sound_Info;
std::vector<std::string> _related_Hero_Info;
std::vector<std::string> _related_Charac_Info;
std::vector<std::string> _related_Object_Info;
std::vector<std::string> _related_Subtitle_Info;

std::vector<std::string> _related_SceneName_Info;

std::vector<std::string> _related_Number_Info;
std::vector<std::string> _related_Transition_Info;

std::vector<std::string> _scene_List;


void TrimString(std::string & str)
{
	if (str.empty())
		return;

	const auto pStr = str.c_str();

	size_t front = 0;
	while (front < str.length() && std::isspace(int(pStr[front]))) { ++front; }

	size_t back = str.length();
	while (back > front && std::isspace(int(pStr[back - 1]))) { --back; }

	if (0 == front)
	{
		if (back < str.length())
		{
			str.resize(back - front);
		}
	}
	else if (back <= front)
	{
		str.clear();
	}
	else
	{
		str = std::move(std::string(str.begin() + front, str.begin() + back));
	}
}

bool isDefaultVal(std::string s)
{
	if (s == "-")
		return true;
	else
		return false;
}

bool isEmptyVal(std::string s)
{
	if (s == "")
		return true;
	else
		return false;
}

bool isNumber(std::string token)
{
	using namespace std;
	return std::regex_match(token, std::regex(("((\\+|-)?[[:digit:]]+)(\\.(([[:digit:]]+)?))?")));
}

bool isFloat(std::string myString) {
	std::istringstream iss(myString);
	float f;
	iss >> noskipws >> f; // noskipws considers leading whitespace invalid
	// Check the entire string was consumed and if either failbit or badbit is set
	return iss.eof() && !iss.fail();
}

std::vector<std::string> getDirectoriesRecursive(const std::string& s)
{
	std::vector<std::string> r;
	for (auto& p : fs::recursive_directory_iterator(s))
		if (p.status().type() == fs::file_type::directory)
			r.push_back(p.path().string());
	return r;
}

std::vector<std::string> getDirectoriesPath(const std::string& s)
{
	std::vector<std::string> r;
	for (auto& p : fs::directory_iterator(s))
		if (p.status().type() == fs::file_type::directory)
			r.push_back(p.path().string());
	return r;
}
std::vector<std::string> getDirectoriesName(const std::string& s)
{
	std::vector<std::string> r;
	for (auto& p : fs::directory_iterator(s))
		if (p.status().type() == fs::file_type::directory)
			r.push_back(p.path().filename().string());
	return r;
}

std::vector<std::string> getFilesPath(const std::string& s)
{
	std::vector<std::string> r;
	for (auto& p : fs::directory_iterator(s))
		if (p.status().type() != fs::file_type::directory)
			r.push_back(p.path().string());
	return r;
}

std::vector<std::string> getFilesName(const std::string& s)
{
	std::vector<std::string> r;
	for (auto& p : fs::directory_iterator(s))
		if (p.status().type() != fs::file_type::directory)
			r.push_back(p.path().filename().string());
	return r;
}


std::vector<std::string> getPNGFilesName(const std::string& s)
{
	std::vector<std::string> r;
	for (auto& p : fs::directory_iterator(s))
		if (p.status().type() != fs::file_type::directory)
		{
			if (p.path().extension() == ".png")
				r.push_back(p.path().filename().string());
		}
	return r;
}

std::vector<std::string> getPNGFilesNameWithInfo(const std::string& s, int& infoIdx)
{
	bool unnecessaryFileExist = false;
	bool unnecessaryFolderExist = false;

	std::vector<std::string> r;
	for (auto& p : fs::directory_iterator(s))
		if (p.status().type() != fs::file_type::directory)
		{
			if (p.path().extension() == ".png")
				r.push_back(p.path().filename().string());
			else
			{
				unnecessaryFileExist = true;
			}
		}
		else
		{
			if (p.path().filename().string() != "sound")
			{
				unnecessaryFolderExist = true;
			}
		}

	if (unnecessaryFileExist == true && unnecessaryFolderExist == true)
	{
		infoIdx = -1;
	}
	else if (unnecessaryFileExist == true)
	{
		infoIdx = -2;
	}
	else if (unnecessaryFolderExist == true)
	{
		infoIdx = -3;
	}
	else
	{
		infoIdx = 0;
	}
	return r;
}


std::vector<std::string> getMP3FilesNameWithInfo(const std::string& s, int& infoIdx)
{
	bool unnecessaryFileExist = false;
	bool unnecessaryFolderExist = false;

	std::vector<std::string> r;
	for (auto& p : fs::directory_iterator(s))
		if (p.status().type() != fs::file_type::directory)
		{
			if (p.path().extension() == ".mp3")
				r.push_back(p.path().filename().string());
			else
			{
				unnecessaryFileExist = true;
			}
		}
		else
		{
			unnecessaryFolderExist = true;
		}

	if (unnecessaryFileExist == true && unnecessaryFolderExist == true)
	{
		infoIdx = -1;
	}
	else if (unnecessaryFileExist == true)
	{
		infoIdx = -2;
	}
	else if (unnecessaryFolderExist == true)
	{
		infoIdx = -3;
	}
	else
	{
		infoIdx = 0;
	}
	return r;
}

std::vector<std::string> getEXTFilesNameWithInfo(const std::string& s, std::string ext, int& infoIdx)
{
	bool unnecessaryFileExist = false;
	bool unnecessaryFolderExist = false;

	std::string extString = "." + ext;

	std::vector<std::string> r;
	for (auto& p : fs::directory_iterator(s))
		if (p.status().type() != fs::file_type::directory)
		{
			if (p.path().extension() == extString)
				r.push_back(p.path().filename().string());
			else
			{
				unnecessaryFileExist = true;
			}
		}
		else
		{
			unnecessaryFolderExist = true;
		}

	if (unnecessaryFileExist == true && unnecessaryFolderExist == true)
	{
		infoIdx = -1;
	}
	else if (unnecessaryFileExist == true)
	{
		infoIdx = -2;
	}
	else if (unnecessaryFolderExist == true)
	{
		infoIdx = -3;
	}
	else
	{
		infoIdx = 0;
	}
	return r;
}

string getExtName(const string& s)
{
	std::string::size_type idx = s.rfind('.');

	return s.substr(idx + 1);
}

string getFileName(const string& s) {

	char sep = '/';

	//#ifdef _WIN32
	//	sep = '\\';
	//#endif

	size_t i = s.rfind(sep, s.length());
	if (i != string::npos) {
		return(s.substr(i + 1, s.length() - i));
	}

	return("");
}

string getFileNameWithOutExt(const string& s) {

	return s.substr(0, s.rfind("."));
}

/**
* @author     BH Park
* @version    2018-11-05
* @brief      Convert Path Separator
* @input	  std::String input_string input string
* @input	  int code 0 : \\ to /, code 1 : / to \\
* @return Number Of Eye Shape Identity
*/
std::string ConvertPathSep(std::string input_string, int code)
{
	std::string output_string;
	if (code == 0)
	{
		for (auto character : input_string)
		{
			if (character == '\\')
			{
				output_string.push_back('/');
			}
			else
			{
				output_string.push_back(character);
			}
		}
	}
	else
	{
		for (auto character : input_string)
		{
			if (character == '/')
			{
				output_string.push_back('\\');
			}
			else
			{
				output_string.push_back(character);
			}
		}
	}
	return output_string;
}

struct Vec3_i
{
	int _x;
	int _y;
	int _z;
	Vec3_i() {
		_x = 0;
		_y = 0;
		_z = 0;
	}
};

struct Vec3_f
{
	float _x;
	float _y;
	float _z;
	Vec3_f() {
		_x = 0.0f;
		_y = 0.0f;
		_z = 0.0f;
	}
};

struct Subtitle
{
	std::string _name;
	std::string _file;
	int _x;
	int _y;
	std::string _font;
	int _font_size;
	std::string _color;
	std::string _font_effect;
	std::string _sound_file;
	int	_sound_volume;
	std::string _sound_option_1;
	std::string _sound_option_2;
	std::string _effect;
	std::string _effect_color;
	std::string _align;

	Subtitle() {
		_name = "-1";
		_file = "-1";
		_x = -1;
		_y = -1;
		_font = "-1";
		_font_size = -1;
		_color = "-1";
		_font_effect = "-1";
		_sound_file = "-1";
		_sound_volume = -1;
		_sound_option_1 = "-1";
		_sound_option_2 = "-1";
		_effect = "-1";
		_effect_color = "-1";
		_align = "-1";
	}
};

struct Background
{
	std::string _name;
	std::string _file;
	int _width;
	int _height;
	Vec3_f _view_port_init;
	Vec3_f _view_port_final;
	float _time;

	Background() {
		_name = "-1";
		_file = "-1";
		_width = -1;
		_height = -1;
		_view_port_init._x = -1;
		_view_port_init._y = -1;
		_view_port_init._z = -1;

		_view_port_final._x = -1;
		_view_port_final._y = -1;
		_view_port_final._z = -1;
		_time = -1;
	}
};

struct Transition
{
	std::string _btn_show;
	std::string _effect;
	std::string _direction;
	std::string _time;
	Transition() {
		_btn_show = "-1";
		_effect = "-1";
		_direction = "-1";
		_time = "-1";
	}
};

struct Sound
{
	int _number;
	std::vector<std::string> _file;
	std::vector<int> _volume;

	Sound() {
		_number = -1;
		_file.clear();
		_volume.clear();
	}
};

struct PositionStack
{
	std::vector<std::string> _sprites;

	PositionStack() {
		_sprites.clear();
	}
};

struct Animation
{
	std::string	_object;
	std::string	_type;
	std::string	_effect;
	std::string	_time;
	std::string	_action;
	std::vector<std::string> _option;

	Animation() {
		_object = "-1";
		_type = "-1";
		_effect = "-1";
		_time = "-1";
		_action = "-1";
		_option.resize(5);
	}
};

struct AnimationStack
{
	std::vector<Animation>	_animations;

	AnimationStack() {
		_animations.clear();
	}
};

struct Trans
{
	float _x0;
	float _y0;
	float _s0;
	float _x1;
	float _y1;
	float _s1;
	float _time;

	Trans() {
		_x0 = -1;
		_y0 = -1;
		_s0 = -1;
		_x1 = -1;
		_y1 = -1;
		_s1 = -1;
		_time = -1;
	}
};

struct Rot
{
	float _angle;
	string _direction;
	float _time;

	Rot() {
		_angle = -1;
		_direction = "-1";
		_time = -1;
	}
};

struct Repeat
{
	int _flag;
	float _time;

	Repeat() {
		_flag = -1;
		_time = -1;
	}
};

struct Effect
{
	string _name;
	string _option_1;
	string _option_2;
	string _option_3;
	float _time;

	Effect() {
		_name = "-1";
		_option_1 = "-1";
		_option_2 = "-1";
		_option_3 = "-1";
		_time = -1;
	}
};

struct Expression
{
	int _left_eye;
	int _right_eye;
	int _left_eye_brow;
	int _right_eye_brow;
	int _mouse;
	int _emotion;

	Expression() {
	}
};

struct Movement
{
	Repeat _repeat;
	std::string _anchor;
	Trans	_trans;
	Rot		_rot;
	Effect	_effect;

	Movement() {
		_anchor = "-1";
	}
};

struct TouchEvent
{
	Sound _sound;
	Effect	_effect;
	Rot		_rot;

	TouchEvent() {
	}
};

struct Sprite
{
	string	_name;
	string	_file;
	int		_x;
	int		_y;
	float	_s;
	int		_width;
	int		_height;

	Movement _movement;
	TouchEvent _touchEvent;

	Sprite() {
		_name = "-1";
		_file = "-1";
		_x = -1;
		_y = -1;
		_s = -1;
		_width = -1;
		_height = -1;
	}
};

struct Face
{
	string	_name;
	string	_file;
	int		_x;
	int		_y;
	float	_s;
	int		_width;
	int		_height;
	string	_shape;
	float	_ang_x;
	float	_ang_y;
	float	_ang_z;

	Movement _movement;
	TouchEvent _touchEvent;
	Expression _expression;

	Face() {
		_name = "-1";
		_file = "-1";
		_x = -1;
		_y = -1;
		_s = -1;
		_width = -1;
		_height = -1;
		_shape = "-1";
		_ang_x = -1;
		_ang_y = -1;
		_ang_z = -1;
	}
};

struct Charac
{
	string _name;
	string _gender;
	string _faceshift;

	Face	_face;
	std::vector<Sprite>	_hair;
	Sprite	_body;
	Sprite	_arm_left;
	Sprite	_arm_right;
	std::vector<Sprite>	_object;

	Movement _movement;
	TouchEvent _touchEvent;

	Charac() {
		_name = "-1";
		_gender = "-1";
		_faceshift = "-1";
		_hair.clear();
		_object.clear();
	}
};

struct Hero
{
	string _name;
	string _gender;
	string _faceshift;
	int		_number;
	std::vector<Charac>	_charac;

	Hero() {
		_name = "-1";
		_gender = "-1";
		_faceshift = "-1";
		_number = -1;
		_charac.clear();
	}
};

struct Scene
{
	std::string _name;
	std::string _scene_name;   // 중복이다 지우자.

	int _hero_num;
	int _charac_num;
	int _object_num;

	int _sprite_num;
	int _animation_num;
	int _subtitle_num;

	Background _background;
	Sound _sound;
	std::vector<Transition> _transition;
	Hero _hero;
	std::vector<Charac> _charac;
	std::vector<Sprite> _object;
	std::vector<Subtitle> _subtitleStack;
	PositionStack _posStack;
	AnimationStack _animationStack;

	Scene() {
		_name = "-1";
		_hero_num = -1;
		_charac_num = -1;
		_object_num = -1;

		_sprite_num = -1;
		_animation_num = -1;
		_subtitle_num = -1;

		_transition.resize(2);
	}
};

int CntNbOfSpecificCharInStr(std::string s, std::string c)
{
	int cnt = 0;
	for (int i = 0; i < s.length() - c.length() + 1; i++)
	{
		bool sameOrNot = true;
		int curr_i = i;
		for (int j = 0; j < c.length(); j++)
		{
			if (c.data()[j] == s.data()[curr_i + j])
			{
				if (i != 0)
				{
					i++;
				}
				continue;
			}
			else
			{
				sameOrNot = false;
				break;
			}
		}

		if (sameOrNot == true)
		{
			cnt++;
		}
	}
	return cnt;
}

int CntNbOfSpecificCharInStrAndGetPos(std::string s, std::string c, std::vector<int>& pos)
{
	int cnt = 0;
	for (int i = 0; i < s.length() - c.length() + 1; i++)
	{
		bool sameOrNot = true;
		int curr_i = i;
		for (int j = 0; j < c.length(); j++)
		{
			if (c.data()[j] == s.data()[curr_i + j])
			{
				if (i != 0)
				{
					i++;
				}
				continue;
			}
			else
			{
				sameOrNot = false;
				break;
			}
		}

		if (sameOrNot == true)
		{
			pos.push_back(i);
			cnt++;
		}
	}
	return cnt;
}

bool ParsingToVec3_i(std::string s, Vec3_i& color)
{
	//Check (r,g,b) type

	std::string pha_0 = "(";
	std::string pha_1 = ")";
	std::string sep = ",";

	std::vector<int> pha_0_pos;
	std::vector<int> pha_1_pos;
	std::vector<int> sep_pos;

	int pha_0_num = CntNbOfSpecificCharInStrAndGetPos(s, pha_0, pha_0_pos);
	int pha_1_num = CntNbOfSpecificCharInStrAndGetPos(s, pha_1, pha_1_pos);
	int sep_num = CntNbOfSpecificCharInStrAndGetPos(s, sep, sep_pos);

	bool colorOrNot;
	if (sep_num == 2 && pha_0_num == 1 && pha_1_num == 1)
	{
		if (pha_0_pos[0] < sep_pos[0] && pha_1_pos[0] > sep_pos[1])
		{
			colorOrNot = true;
		}
		else
		{
			colorOrNot = false;
		}
	}
	else
		colorOrNot = false;

	if (colorOrNot == false) {
		return 	colorOrNot;
	}
	else
	{
		std::string x_str = s.substr(pha_0_pos[0] + 1, sep_pos[0] - pha_0_pos[0] - 2);
		std::string y_str = s.substr(sep_pos[0], sep_pos[1] - sep_pos[0] - 1);
		std::string z_str = s.substr(sep_pos[1], pha_1_pos[0] - sep_pos[1] - 1);

		TrimString(x_str);
		TrimString(y_str);
		TrimString(z_str);

		color._x = std::stoi(x_str);
		color._y = std::stoi(y_str);
		color._z = std::stoi(z_str);
		return 	colorOrNot;
	}
}

bool ParsingToVec3_f(std::string s, Vec3_f& color)
{
	//Check (r,g,b) type

	std::string pha_0 = "(";
	std::string pha_1 = ")";
	std::string sep = ",";

	std::vector<int> pha_0_pos;
	std::vector<int> pha_1_pos;
	std::vector<int> sep_pos;

	int pha_0_num = CntNbOfSpecificCharInStrAndGetPos(s, pha_0, pha_0_pos);
	int pha_1_num = CntNbOfSpecificCharInStrAndGetPos(s, pha_1, pha_1_pos);
	int sep_num = CntNbOfSpecificCharInStrAndGetPos(s, sep, sep_pos);

	bool colorOrNot;
	if (sep_num == 2 && pha_0_num == 1 && pha_1_num == 1)
	{
		if (pha_0_pos[0] < sep_pos[0] && pha_1_pos[0] > sep_pos[1])
		{
			colorOrNot = true;
		}
		else
		{
			colorOrNot = false;
		}
	}
	else
		colorOrNot = false;

	if (colorOrNot == false) {
		return 	colorOrNot;
	}
	else
	{
		std::string x_str = s.substr(pha_0_pos[0] + 1, sep_pos[0] - pha_0_pos[0] - 2);
		std::string y_str = s.substr(sep_pos[0], sep_pos[1] - sep_pos[0] - 1);
		std::string z_str = s.substr(sep_pos[1], pha_1_pos[0] - sep_pos[1] - 1);

		TrimString(x_str);
		TrimString(y_str);
		TrimString(z_str);

		color._x = std::stof(x_str);
		color._y = std::stof(y_str);
		color._z = std::stof(z_str);
		return 	colorOrNot;
	}
}

bool isVec3(std::string s)
{
	//Check (r,g,b) type

	std::string pha_0 = "(";
	std::string pha_1 = ")";
	std::string sep = ",";

	std::vector<int> pha_0_pos;
	std::vector<int> pha_1_pos;
	std::vector<int> sep_pos;

	int pha_0_num = CntNbOfSpecificCharInStrAndGetPos(s, pha_0, pha_0_pos);
	int pha_1_num = CntNbOfSpecificCharInStrAndGetPos(s, pha_1, pha_1_pos);
	int sep_num = CntNbOfSpecificCharInStrAndGetPos(s, sep, sep_pos);

	bool colorOrNot;
	if (sep_num == 2 && pha_0_num == 1 && pha_1_num == 1)
	{
		if (pha_0_pos[0] < sep_pos[0] && pha_1_pos[0] > sep_pos[1])
		{
			colorOrNot = true;
		}
		else
		{
			colorOrNot = false;
		}
	}
	else
		colorOrNot = false;

	if (colorOrNot == false) {
		return 	colorOrNot;
	}
	else
	{
		std::string x_str = s.substr(pha_0_pos[0] + 1, sep_pos[0] - pha_0_pos[0] - 2);
		std::string y_str = s.substr(sep_pos[0], sep_pos[1] - sep_pos[0] - 1);
		std::string z_str = s.substr(sep_pos[1], pha_1_pos[0] - sep_pos[1] - 1);

		TrimString(x_str);
		TrimString(y_str);
		TrimString(z_str);

		if (isNumber(x_str) == false)
		{
			return false;
		}
		if (isNumber(y_str) == false)
		{
			return false;
		}
		if (isNumber(z_str) == false)
		{
			return false;
		}

		return 	colorOrNot;
	}
}

bool isVec4(std::string s)
{
	//Check (r,g,b) type

	std::string pha_0 = "(";
	std::string pha_1 = ")";
	std::string sep = ",";

	std::vector<int> pha_0_pos;
	std::vector<int> pha_1_pos;
	std::vector<int> sep_pos;

	int pha_0_num = CntNbOfSpecificCharInStrAndGetPos(s, pha_0, pha_0_pos);
	int pha_1_num = CntNbOfSpecificCharInStrAndGetPos(s, pha_1, pha_1_pos);
	int sep_num = CntNbOfSpecificCharInStrAndGetPos(s, sep, sep_pos);

	bool colorOrNot;
	if (sep_num == 3 && pha_0_num == 1 && pha_1_num == 1)
	{
		if (pha_0_pos[0] < sep_pos[0] && pha_1_pos[0] > sep_pos[2])
		{
			colorOrNot = true;
		}
		else
		{
			colorOrNot = false;
		}
	}
	else
		colorOrNot = false;

	if (colorOrNot == false) {
		return 	colorOrNot;
	}
	else
	{
		//std::string x_str = s.substr(pha_0_pos[0] + 1, sep_pos[0] - pha_0_pos[0] - 2);
		//std::string y_str = s.substr(sep_pos[0] + 1, sep_pos[1] - sep_pos[0] - 2);
		//std::string z_str = s.substr(sep_pos[1] + 1, sep_pos[2] - sep_pos[1] - 2);
		//std::string k_str = s.substr(sep_pos[2] + 1, pha_1_pos[0] - sep_pos[2] - 2);

		std::string x_str = s.substr(pha_0_pos[0] + 1, sep_pos[0] - pha_0_pos[0] - 2);
		std::string y_str = s.substr(sep_pos[0], sep_pos[1] - sep_pos[0] - 1);
		std::string z_str = s.substr(sep_pos[1], sep_pos[2] - sep_pos[1] - 1);
		std::string k_str = s.substr(sep_pos[2], pha_1_pos[0] - sep_pos[2] - 1);

		TrimString(x_str);
		TrimString(y_str);
		TrimString(z_str);
		TrimString(k_str);

		if (isNumber(x_str) == false)
		{
			return false;
		}
		if (isNumber(y_str) == false)
		{
			return false;
		}
		if (isNumber(z_str) == false)
		{
			return false;
		}
		if (isNumber(k_str) == false)
		{
			return false;
		}

		return 	colorOrNot;
	}
}


bool EmptyCharacOrNot(Charac charac);
void IgnoreTouchEvent(TouchEvent& touch);

void ReadSubtitle(xml_node<>* node, int& subtitleNum, std::vector<Subtitle>& subtitleStack);
void ReadBackground(xml_node<>* node, Background& background);
void ReadPositionStack(xml_node<>* node, PositionStack& posStack);
void ReadAnimationStack(xml_node<>* node, AnimationStack& posStack);
void ReadSound(xml_node<>* node, Sound& sound);
void ReadTransition(xml_node<>* node, std::vector<Transition>& transition);
void ReadCharac(xml_node<>* node, std::vector<Charac>& charac);
void ReadHero(xml_node<>* node, Hero& hero);
void ReadObjects(xml_node<>* node, std::vector<Sprite>& object);
void ReadObject(xml_node<>* node, Sprite& object);
void ReadMovement(xml_node<>* node, Movement& movement);
void ReadTouchEvent(xml_node<>* node, TouchEvent& touchEvent);
void ReadTrans(xml_node<>* node, Trans& trans);
void ReadRot(xml_node<>* node, Rot& rot);
void ReadEffect(xml_node<>* node, Effect& effect);
void ReadRepeat(xml_node<>* node, Repeat& repeat);
void ReadExpression(xml_node<>* node, Expression& expression);
bool ReadFace(xml_node<>* node, Face& sprite);

void Validate_SceneName(std::vector<std::string> scene);
void Validate_FileName(Scene scene);

void Validate_Value(Scene scene);
void Validate_FileSizeValue(Scene scene);
void Validate_PositionStack(Scene scene);

void Validate_Anchor(Scene scene);
//void Validate_Trans(Scene scene);
void Validate_Rot(Scene scene);
void Validate_Effect(Scene scene);
void Validate_Animation(Scene scene);
void Validate_Subtitle(Scene scene);
void Validate_Sound(Scene scene);
void Validate_Movement(Scene scene);

void Validate_Hero_Value(Hero hero);
void Validate_Charac_Value(Charac charac, string characName);
void Validate_Face_Value(Face sprite, string objectName);
void Validate_Sprite_Value(Sprite sprite, string objectName);
void Validate_TouchEvent_Value(TouchEvent touchEvent, string objectName);
void Validate_Movement_Value(Movement touchEvent, string objectName);
void Validate_Subtitle_Value(Subtitle subtitle, string objectName);
void Validate_Background_Value(Scene scene);

void Validate_Number_Value(Scene scene);
void Validate_Transition(Scene scene);

std::string curr_group_name;
std::string curr_object_name;

void main()
{
	std::string currentPath = ConvertPathSep(fs::current_path().string(), 0);
	std::vector<std::string> rootSubFolders;
	std::vector<std::string> sceneNames;

	std::vector<std::string> trgSpecFilePath;
	std::vector<std::string> squeezed_trg_spec_file_path;

	///////////////////////////////////////////////////////////////
	//	넘겨줄 때는 이 아래부분 주석처리 (local folder)

	TCHAR user_name[MAX_PATH] = { 0, };
	DWORD user_size = MAX_PATH;
	GetUserName(user_name, &user_size);

	TCHAR user_directory[MAX_PATH] = { 0, };
	GetWindowsDirectory(user_directory, MAX_PATH);

	//    need folder name ..
	TCHAR convert_directory[MAX_PATH] = { "pinocchio" };
	TCHAR user_current_path[MAX_PATH] = { 0 , };
	sprintf_s(user_current_path, MAX_PATH,
		"%c:\\Users\\%s\\AppData\\LocalLow\\argo\\StorySelf\\storyself\\content\\europe\\%s", user_directory[0], user_name, convert_directory);

	currentPath = user_current_path;

	/////////////////////////////////////////////////////

	//    Result string data
	
	rootSubFolders = getDirectoriesName(currentPath);

	//Get Scene Names
	int thumbnail_idx = -1;
	int currIdx = 0;
	for (int i = 0; i < rootSubFolders.size(); i++)
	{
		std::string folder_preFix = rootSubFolders[i].substr(0, 6);
		if (folder_preFix == "Scene_")
		{
			sceneNames.push_back(rootSubFolders[i]);

			size_t idx = rootSubFolders[i].rfind("_");
			std::string sceneNum = rootSubFolders[i].substr(idx + 1, rootSubFolders[i].length() - idx);
			std::string specFilePath = currentPath + "/" + sceneNames[currIdx] + "/scene_" + sceneNum + ".xml";
			std::string squeezed_spec_file_path = currentPath + "/" + sceneNames[currIdx] + "/scene_" + sceneNum + "_sqzd.xml";

			trgSpecFilePath.push_back(specFilePath);
			squeezed_trg_spec_file_path.push_back(squeezed_spec_file_path);
			currIdx++;
		}
		else if (rootSubFolders[i] == "thumbnail")
		{
			std::string specFilePath = currentPath + "/thumbnail/thumbnail.xml";
			std::string squeezed_spec_file_path = currentPath + "/thumbnail/thumbnail_sqzd.xml";

			trgSpecFilePath.push_back(specFilePath);
			squeezed_trg_spec_file_path.push_back(squeezed_spec_file_path);
			thumbnail_idx = i;
		}
	}

	std::cout << "==== StorySelf Contents Spec Validator v0.1 ====" << std::endl;
	std::cout << std::endl;
	std::cout << std::endl;

	//_scene_List.resize(trgSpecFilePath.size());

	for (int sceneNum = 0; sceneNum < trgSpecFilePath.size(); sceneNum++)
	{
		if (fs::exists(trgSpecFilePath[sceneNum].c_str()) == true)
		{
			_related_Background_Info.clear();
			_related_FileName_Info.clear();
			_related_FileSizeValue_Info.clear();
			_related_PosStack_Info.clear();
			_related_Anchor_Info.clear();
			_related_Rot_Info.clear();
			_related_AniStack_Info.clear();
			_related_SceneName_Info.clear();
			_related_Value_Info.clear();
			_related_Sound_Info.clear();
			_related_Number_Info.clear();
			_related_Transition_Info.clear();
			_related_Effect_Info.clear();
			Scene _scene;
			xml_document<> doc;
			std::ifstream file(trgSpecFilePath[sceneNum].c_str());
			std::stringstream buffer;
			buffer << file.rdbuf();
			file.close();
			std::string content(buffer.str());
			doc.parse<0>(&content[0]);

			xml_node<> *pRoot = doc.first_node();
			// With the xml example above this is the 

			for (xml_node<> *pNode = pRoot->first_node(); pNode; pNode = pNode->next_sibling())
			{
				std::string categoryName = pNode->name();
				// Do something here
				if (categoryName == "scenario")
				{
					for (xml_node<> *pSubNode = pNode->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
					{
						std::string subCategoryName = pSubNode->name();

						if (subCategoryName == "name")
						{
							std::string subCategoryValue = pSubNode->value();
							//_scene_List[sceneNum] = subCategoryValue;
							_scene_List.push_back(subCategoryValue);
						}
						else if (subCategoryName == "position_stack")
						{
							ReadPositionStack(pSubNode, _scene._posStack);
						}
						else if (subCategoryName == "animation_stack")
						{
							ReadAnimationStack(pSubNode, _scene._animationStack);
						}
						else if (subCategoryName == "hero_num")
						{
							std::string subCategoryValue = pSubNode->value();							
							if (!isDefaultVal(subCategoryValue))
								_scene._hero_num = std::stoi(subCategoryValue);
							else
							{
								std::string info = "Error : hero_num";
								info = info + " -> It should be specified as a number";
								_related_Value_Info.push_back(info);
								_scene._hero_num = -2;
							}
						}
						else if (subCategoryName == "charac_num")
						{
							std::string subCategoryValue = pSubNode->value();
							if (!isDefaultVal(subCategoryValue))
								_scene._charac_num = std::stoi(subCategoryValue);
							else
							{
								std::string info = "Error : charac_num";
								info = info + " -> It should be specified as a number";
								_related_Value_Info.push_back(info);
								_scene._charac_num = -2;
							}
						}
						else if (subCategoryName == "object_num")
						{
							std::string subCategoryValue = pSubNode->value();
							if (!isDefaultVal(subCategoryValue))
								_scene._object_num = std::stoi(subCategoryValue);
							else
							{
								std::string info = "Error : object_num";
								info = info + " -> It should be specified as a number";
								_related_Value_Info.push_back(info);
								_scene._object_num = -2;
							}
						}
						else if (subCategoryName == "sprite_num")
						{
							std::string subCategoryValue = pSubNode->value();
							if (!isDefaultVal(subCategoryValue))
								_scene._sprite_num = std::stoi(subCategoryValue);
							else
							{
								std::string info = "Error : sprite_num";
								info = info + " -> It should be specified as a number";
								_related_Value_Info.push_back(info);
								_scene._sprite_num = -2;
							}
						}
						else if (subCategoryName == "animation_num")
						{
							std::string subCategoryValue = pSubNode->value();
							_scene._animation_num = std::stoi(subCategoryValue);
						}
						else if (subCategoryName == "subtitle_num")
						{
							std::string subCategoryValue = pSubNode->value();
							_scene._subtitle_num = std::stoi(subCategoryValue);
						}
					}
				}
				else if (categoryName == "subtitle")
				{
					ReadSubtitle(pNode, _scene._subtitle_num, _scene._subtitleStack);
				}
				else if (categoryName == "background")
				{
					ReadBackground(pNode, _scene._background);
				}
				else if (categoryName == "sound")
				{
					ReadSound(pNode, _scene._sound);
				}
				else if (categoryName == "transition")
				{
					ReadTransition(pNode, _scene._transition);
				}
				else if (categoryName == "hero")
				{
					ReadHero(pNode, _scene._hero);
				}
				else if (categoryName == "charac")
				{
					ReadCharac(pNode, _scene._charac);
				}
				else if (categoryName == "object")
				{
					ReadObjects(pNode, _scene._object);
				}
			}

			//1. Position Stack Validate
			Validate_FileName(_scene);
			Validate_FileSizeValue(_scene);
			Validate_Value(_scene);
			Validate_PositionStack(_scene);
			Validate_Anchor(_scene);
			Validate_Rot(_scene);
			Validate_Animation(_scene);
			Validate_Subtitle(_scene);
			Validate_Sound(_scene);
			Validate_SceneName(_scene_List);

			Validate_Number_Value(_scene);

			if (thumbnail_idx == sceneNum)
			{
				Validate_Transition(_scene);
			}

			std::cout << std::endl;
			std::cout << "----------------------------------------------------------------------------------------------------------" << std::endl;
			std::cout << std::endl;
			std::cout << trgSpecFilePath[sceneNum] << " Validating..." << std::endl;
			std::cout << std::endl;
			std::cout << std::endl;

			for (auto p : _related_Background_Info) {
				std::cout << p << std::endl;
			}
			for (int i = 0; i < _related_FileName_Info.size(); i++)
			{
				std::cout << _related_FileName_Info[i] << std::endl;
			}
			for (int i = 0; i < _related_FileSizeValue_Info.size(); i++)
			{
				std::cout << _related_FileSizeValue_Info[i] << std::endl;
			}
			for (int i = 0; i < _related_PosStack_Info.size(); i++)
			{
				std::cout << _related_PosStack_Info[i] << std::endl;
			}
			for (int i = 0; i < _related_Anchor_Info.size(); i++)
			{
				std::cout << _related_Anchor_Info[i] << std::endl;
			}
			for (int i = 0; i < _related_Rot_Info.size(); i++)
			{
				std::cout << _related_Rot_Info[i] << std::endl;
			}
			for (int i = 0; i < _related_AniStack_Info.size(); i++)
			{
				std::cout << _related_AniStack_Info[i] << std::endl;
			}
			for (int i = 0; i < _related_Subtitle_Info.size(); i++)
			{
				std::cout << _related_Subtitle_Info[i] << std::endl;
			}
			for (int i = 0; i < _related_Sound_Info.size(); i++)
			{
				std::cout << _related_Sound_Info[i] << std::endl;
			}

			for (int i = 0; i < _related_Value_Info.size(); i++)
			{
				std::cout << _related_Value_Info[i] << std::endl;
			}

			for (int i = 0; i < _related_SceneName_Info.size(); i++)
			{
				std::cout << _related_SceneName_Info[i] << std::endl;
			}

			for (int i = 0; i < _related_Number_Info.size(); i++)
			{
				std::cout << _related_Number_Info[i] << std::endl;
			}

			for (int i = 0; i < _related_Transition_Info.size(); i++)
			{
				std::cout << _related_Transition_Info[i] << std::endl;
			}

			for (int i = 0; i < _related_Effect_Info.size(); i++)
			{
				std::cout << _related_Effect_Info[i] << std::endl;
			}

			std::cout << std::endl;
			std::cout << trgSpecFilePath[sceneNum] << " Validating... Done." << std::endl;
			std::cout << std::endl;
			std::cout << "----------------------------------------------------------------------------------------------------------" << std::endl;
			std::cout << std::endl;
		}
		else
		{
			std::cout << trgSpecFilePath[sceneNum] << " is Not Found!!" << std::endl;
		}
	}


	std::cout << std::endl;
	std::cout << std::endl;
	system("PAUSE");
	std::cout << "Press Any Key To Exit" << std::endl;

}

void Validate_FileName(Scene scene)
{
	std::string fileName;
	std::string ext;
	//bgr
	fileName = scene._background._file;
	if (fileName != "-1" && fileName != "-2")
	{
		ext = getExtName(scene._background._file);
		if (ext != "png")
		{
			std::string info = "Error : " + scene._background._file;
			info = info + " -> File name should have png extension";

			_related_FileName_Info.push_back(info);
		}
	}

	//hero
	for (int i = 0; i < scene._hero._charac.size(); i++)
	{
		fileName = scene._hero._charac[i]._face._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";

				_related_FileName_Info.push_back(info);
			}
		}
		////////

		fileName = scene._hero._charac[i]._body._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";
				_related_FileName_Info.push_back(info);
			}
		}
		////////
		for (int j = 0; j < scene._hero._charac[i]._hair.size(); j++)
		{
			fileName = scene._hero._charac[i]._hair[j]._file;
			if (fileName != "-1" && fileName != "-2")
			{
				ext = getExtName(fileName);
				if (ext != "png")
				{
					std::string info = "Error : " + fileName;
					info = info + " -> File name should have png extension";
					_related_FileName_Info.push_back(info);
				}
			}
		}
		////////

		fileName = scene._hero._charac[i]._body._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";
				_related_FileName_Info.push_back(info);
			}
		}
		////////

		fileName = scene._hero._charac[i]._arm_left._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";
				_related_FileName_Info.push_back(info);
			}
		}
		////////

		fileName = scene._hero._charac[i]._arm_right._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";
				_related_FileName_Info.push_back(info);
			}
		}
	}
	//charac
	for (int i = 0; i < scene._charac.size(); i++)
	{
		fileName = scene._charac[i]._face._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";

				_related_FileName_Info.push_back(info);
			}
		}

		fileName = scene._charac[i]._body._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";
				_related_FileName_Info.push_back(info);
			}
		}
		////////
		for (int j = 0; j < scene._charac[i]._hair.size(); j++)
		{
			fileName = scene._charac[i]._hair[j]._file;
			if (fileName != "-1" && fileName != "-2")
			{
				ext = getExtName(fileName);
				if (ext != "png")
				{
					std::string info = "Error : " + fileName;
					info = info + " -> File name should have png extension";
					_related_FileName_Info.push_back(info);
				}
			}
		}
		////////
		fileName = scene._charac[i]._body._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";
				_related_FileName_Info.push_back(info);
			}
		}
		////////

		fileName = scene._charac[i]._arm_left._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";
				_related_FileName_Info.push_back(info);
			}
		}
		////////
		fileName = scene._charac[i]._arm_right._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";
				_related_FileName_Info.push_back(info);
			}
		}
	}
	//object
	for (int i = 0; i < scene._object.size(); i++)
	{
		fileName = scene._object[i]._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "png")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have png extension";

				_related_FileName_Info.push_back(info);
			}
		}
	}
	//subtitle
	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		fileName = scene._subtitleStack[i]._file;
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "txt")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have txt extension";

				_related_FileName_Info.push_back(info);
			}
		}
	}

	//sound - bgr
	for (int i = 0; i < scene._sound._file.size(); i++)
	{
		fileName = scene._sound._file[i];
		if (fileName != "-1" && fileName != "-2")
		{
			ext = getExtName(fileName);
			if (ext != "mp3" && ext != "wav" && fileName != "continue")
			{
				std::string info = "Error : " + fileName;
				info = info + " -> File name should have 'mp3' or 'wav' extension or 'continue'";

				_related_FileName_Info.push_back(info);
			}
		}
	}
	//sound - hero
	for (int i = 0; i < scene._hero._charac.size(); i++)
	{
		for (int j = 0; j < scene._hero._charac[i]._touchEvent._sound._file.size(); j++)
		{
			fileName = scene._hero._charac[i]._touchEvent._sound._file[j];
			if (fileName != "-1" && fileName != "-2")
			{
				ext = getExtName(fileName);
				if (ext != "mp3" && ext != "wav")
				{
					std::string info = "Error : " + fileName;
					info = info + " -> File name should have 'mp3' or 'wav' extension, on " + scene._hero._name;

					_related_FileName_Info.push_back(info);
				}
			}
		}
	}
	//sound - charac
	for (int i = 0; i < scene._charac.size(); i++)
	{
		for (int j = 0; j < scene._charac[i]._touchEvent._sound._file.size(); j++)
		{
			fileName = scene._charac[i]._touchEvent._sound._file[j];
			if (fileName != "-1" && fileName != "-2")
			{
				ext = getExtName(fileName);
				if (ext != "mp3" && ext != "wav")
				{
					std::string info = "Error : " + fileName;
					info = info + " -> File name should have mp3 or wav extension, on " + scene._charac[i]._name;

					_related_FileName_Info.push_back(info);
				}
			}
		}
	}
	//sound - object
	for (int i = 0; i < scene._object.size(); i++)
	{
		for (int j = 0; j < scene._object[i]._touchEvent._sound._file.size(); j++)
		{
			fileName = scene._object[i]._touchEvent._sound._file[j];
			if (fileName != "-1" && fileName != "-2")
			{
				ext = getExtName(fileName);
				if (ext != "mp3" && ext != "wav")
				{
					std::string info = "Error : " + fileName;
					info = info + " -> File name should have mp3 or wav extension, on " + scene._object[i]._name;

					_related_FileName_Info.push_back(info);
				}
			}
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////////
}



void Validate_FileSizeValue(Scene scene)
{
	std::string fileName;
	std::string ext;
	//bgr

	if (scene._background._file != "-1" && scene._background._file != "-2")
	{
		if (scene._background._width < 0)
		{
			std::string info = "Error : " + scene._background._file;
			info = info + " -> Background Object width should be specified!";

			_related_FileSizeValue_Info.push_back(info);
		}

		if (scene._background._height < 0)
		{
			std::string info = "Error : " + scene._background._file;
			info = info + " -> Background Object height should be specified!";

			_related_FileSizeValue_Info.push_back(info);
		}
	}


	//hero
	for (int i = 0; i < scene._hero._charac.size(); i++)
	{
		if (scene._hero._charac[i]._face._file != "-1" && scene._hero._charac[i]._face._file != "-2")
		{
			if (scene._hero._charac[i]._face._width < 0)
			{
				std::string info = "Error : " + scene._hero._charac[i]._face._file + " on hero_" + std::to_string(i + 1);
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._hero._charac[i]._face._height < 0)
			{
				std::string info = "Error : " + scene._hero._charac[i]._face._file + " on hero_" + std::to_string(i + 1);
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
		////////
		for (int j = 0; j < scene._hero._charac[i]._hair.size(); j++)
		{
			if (scene._hero._charac[i]._hair[j]._file != "-1" && scene._hero._charac[i]._hair[j]._file != "-2")
			{
				if (scene._hero._charac[i]._hair[j]._width < 0)
				{
					std::string info = "Error : " + scene._hero._charac[i]._hair[j]._file + " on hero_" + std::to_string(i + 1);
					info = info + " -> Object width should be specified!";

					_related_FileSizeValue_Info.push_back(info);
				}

				if (scene._hero._charac[i]._hair[j]._height < 0)
				{
					std::string info = "Error : " + scene._hero._charac[i]._hair[j]._file + " on hero_" + std::to_string(i + 1);
					info = info + " -> Object height should be specified!";

					_related_FileSizeValue_Info.push_back(info);
				}
			}
		}
		////////
		if (scene._hero._charac[i]._body._file != "-1" && scene._hero._charac[i]._body._file != "-2")
		{
			if (scene._hero._charac[i]._body._width < 0)
			{
				std::string info = "Error : " + scene._hero._charac[i]._body._file + " on hero_" + std::to_string(i + 1);
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._hero._charac[i]._body._height < 0)
			{
				std::string info = "Error : " + scene._hero._charac[i]._body._file + " on hero_" + std::to_string(i + 1);
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
		////////
		if (scene._hero._charac[i]._arm_left._file != "-1" && scene._hero._charac[i]._arm_left._file != "-2")
		{
			if (scene._hero._charac[i]._arm_left._width < 0)
			{
				std::string info = "Error : " + scene._hero._charac[i]._arm_left._file + " on hero_" + std::to_string(i + 1);
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._hero._charac[i]._arm_left._height < 0)
			{
				std::string info = "Error : " + scene._hero._charac[i]._arm_left._file + " on hero_" + std::to_string(i + 1);
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
		////////
		if (scene._hero._charac[i]._arm_right._file != "-1" && scene._hero._charac[i]._arm_right._file != "-2")
		{
			if (scene._hero._charac[i]._arm_right._width < 0)
			{
				std::string info = "Error : " + scene._hero._charac[i]._arm_right._file + " on hero_" + std::to_string(i + 1);
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._hero._charac[i]._arm_right._height < 0)
			{
				std::string info = "Error : " + scene._hero._charac[i]._arm_right._file + " on hero_" + std::to_string(i + 1);
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
	}
	//charac
	for (int i = 0; i < scene._charac.size(); i++)
	{
		if (scene._charac[i]._face._file != "-1" && scene._charac[i]._face._file != "-2")
		{
			if (scene._charac[i]._face._width < 0)
			{
				std::string info = "Error : " + scene._charac[i]._face._file + " on " + scene._charac[i]._name;
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._charac[i]._face._height < 0)
			{
				std::string info = "Error : " + scene._charac[i]._face._file + " on " + scene._charac[i]._name;
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
		////////
		for (int j = 0; j < scene._charac[i]._hair.size(); j++)
		{
			if (scene._charac[i]._hair[j]._file != "-1" && scene._charac[i]._hair[j]._file != "-2")
			{
				if (scene._charac[i]._hair[j]._width < 0)
				{
					std::string info = "Error : " + scene._charac[i]._hair[j]._file + " on " + scene._charac[i]._name;
					info = info + " -> Object width should be specified!";

					_related_FileSizeValue_Info.push_back(info);
				}

				if (scene._charac[i]._hair[j]._height < 0)
				{
					std::string info = "Error : " + scene._charac[i]._hair[j]._file + " on " + scene._charac[i]._name;
					info = info + " -> Object height should be specified!";

					_related_FileSizeValue_Info.push_back(info);
				}
			}
		}
		////////
		if (scene._charac[i]._body._file != "-1" && scene._charac[i]._body._file != "-2")
		{
			if (scene._charac[i]._body._width < 0)
			{
				std::string info = "Error : " + scene._charac[i]._body._file + " on " + scene._charac[i]._name;
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._charac[i]._body._height < 0)
			{
				std::string info = "Error : " + scene._charac[i]._body._file + " on " + scene._charac[i]._name;
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
		////////
		if (scene._charac[i]._arm_left._file != "-1" && scene._charac[i]._arm_left._file != "-2")
		{
			if (scene._charac[i]._arm_left._width < 0)
			{
				std::string info = "Error : " + scene._charac[i]._arm_left._file + " on " + scene._charac[i]._name;
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._charac[i]._arm_left._height < 0)
			{
				std::string info = "Error : " + scene._charac[i]._arm_left._file + " on " + scene._charac[i]._name;
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
		////////
		if (scene._charac[i]._arm_right._file != "-1" && scene._charac[i]._arm_right._file != "-2")
		{
			if (scene._charac[i]._arm_right._width < 0)
			{
				std::string info = "Error : " + scene._charac[i]._arm_right._file + " on " + scene._charac[i]._name;
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._charac[i]._arm_right._height < 0)
			{
				std::string info = "Error : " + scene._charac[i]._arm_right._file + " on " + scene._charac[i]._name;
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
	}
	//object
	for (int i = 0; i < scene._object.size(); i++)
	{
		if (scene._object[i]._file != "-1" && scene._object[i]._file != "-2")
		{
			if (scene._object[i]._width < 0)
			{
				std::string info = "Error : " + scene._object[i]._file + " on " + scene._object[i]._name;
				info = info + " -> Object width should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}

			if (scene._object[i]._height < 0)
			{
				std::string info = "Error : " + scene._object[i]._file + " on " + scene._object[i]._name;
				info = info + " -> Object height should be specified!";

				_related_FileSizeValue_Info.push_back(info);
			}
		}
	}
}

void Validate_Transition(Scene scene)
{
	if (scene._transition[0]._btn_show != "true" && scene._transition[0]._btn_show != "false")
	{
		std::string info = "Error : Transition Prev btn_show should be true or false!!";
		_related_Transition_Info.push_back(info);
	}

	if (scene._transition[0]._effect != "fade" &&
		scene._transition[0]._effect != "push" &&
		scene._transition[0]._effect != "cover" &&
		scene._transition[0]._effect != "uncover" &&
		scene._transition[0]._effect != "rotate" &&
		scene._transition[0]._effect != "wipe")
	{
		std::string info = "Error : Transition Prev Effect should be one of (fade, push, cover, uncover, rotate, wipe)!!";
		_related_Transition_Info.push_back(info);
	}

	if (scene._transition[0]._direction != "left_from_right" &&
		scene._transition[0]._direction != "right_from_left" &&
		scene._transition[0]._direction != "top_from_bottom" &&
		scene._transition[0]._direction != "bottom_from_top" &&
		scene._transition[0]._direction != "-2")
	{
		std::string info = "Error : Transition Prev Direction should be (-) or one of (left_from_right, right_from_left, top_from_bottom, bottom_from_top) when effect is one of (push, cover, uncover, wipe)!!";
		_related_Transition_Info.push_back(info);
	}

	if (isFloat(scene._transition[0]._time) == false)
	{
		std::string info = "Error : Transition Prev Time should be number!!";
		_related_Transition_Info.push_back(info);
	}

	if (scene._transition[1]._btn_show != "true" && scene._transition[1]._btn_show != "false")
	{
		std::string info = "Error : Transition Next btn_show should be true or false!!";
		_related_Transition_Info.push_back(info);
	}

	if (scene._transition[1]._effect != "fade" &&
		scene._transition[1]._effect != "push" &&
		scene._transition[1]._effect != "cover" &&
		scene._transition[1]._effect != "uncover" &&
		scene._transition[1]._effect != "rotate" &&
		scene._transition[1]._effect != "wipe")
	{
		std::string info = "Error : Transition Next Effect should be one of (fade, push, cover, uncover, rotate, wipe)!!";
		_related_Transition_Info.push_back(info);
	}

	if (scene._transition[1]._direction != "left_from_right" &&
		scene._transition[1]._direction != "right_from_left" &&
		scene._transition[1]._direction != "top_from_bottom" &&
		scene._transition[1]._direction != "bottom_from_top" &&
		scene._transition[1]._direction != "-2")
	{
		std::string info = "Error : Transition Next Direction should be (-) or one of (left_from_right, right_from_left, top_from_bottom, bottom_from_top) when effect is one of (push, cover, uncover, wipe)!!";
		_related_Transition_Info.push_back(info);
	}

	if (isFloat(scene._transition[1]._time) == false)
	{
		std::string info = "Error : Transition Next Time should be number!!";
		_related_Transition_Info.push_back(info);
	}
}

void Validate_Number_Value(Scene scene)
{
	if (scene._hero_num != scene._hero._charac.size())
	{
		std::string info = "Error : Hero Num on Scenario (" + std::to_string(scene._hero_num) + ") and Real Hero Num (" + std::to_string(scene._hero._charac.size()) + ") is Different!!";
		_related_Number_Info.push_back(info);
	}

	if (scene._charac_num != scene._charac.size())
	{
		std::string info = "Error : Charac Num on Scenario (" + std::to_string(scene._charac_num) + ") and Real Charac Num (" + std::to_string(scene._charac.size()) + ") is Different!!";
		_related_Number_Info.push_back(info);
	}

	if (scene._object_num != scene._object.size())
	{
		std::string info = "Error : Object Num on Scenario (" + std::to_string(scene._object_num) + ") and Real Object Num (" + std::to_string(scene._object.size()) + ") is Different!!";
		_related_Number_Info.push_back(info);
	}

	if (scene._subtitle_num != scene._subtitleStack.size())
	{
		std::string info = "Error : Subtitle Num on Scenario (" + std::to_string(scene._subtitle_num) + ") and Real Subtitle Num (" + std::to_string(scene._subtitleStack.size()) + ") is Different!!";
		_related_Number_Info.push_back(info);
	}

	if (scene._animation_num != scene._animationStack._animations.size())
	{
		std::string info = "Error : Animation Num on Scenario (" + std::to_string(scene._animation_num) + ") and Real Animation Num (" + std::to_string(scene._animationStack._animations.size()) + ") is Different!!";
		_related_Number_Info.push_back(info);
	}

	if (scene._sprite_num != scene._posStack._sprites.size())
	{
		std::string info = "Error : Sprite Num on Scenario (" + std::to_string(scene._sprite_num) + ") and Position Stack Size (" + std::to_string(scene._posStack._sprites.size()) + ") is Different!!";
		_related_Number_Info.push_back(info);
	}
}

void Validate_Value(Scene scene)
{
	//Background
	Validate_Background_Value(scene);

	//hero
	Validate_Hero_Value(scene._hero);

	//charac
	for (int i = 0; i < scene._charac.size(); i++)
	{
		Validate_Charac_Value(scene._charac[i], scene._charac[i]._name);
	}

	//object
	for (int i = 0; i < scene._object.size(); i++)
	{
		Validate_Sprite_Value(scene._object[i], scene._object[i]._name);
	}

	//subtitle
	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		Validate_Subtitle_Value(scene._subtitleStack[i], scene._subtitleStack[i]._name);
	}
}



void Validate_PositionStack(Scene scene)
{
	std::string fileName;
	std::string name;
	std::string ext;

	std::vector<std::string> spriteList;

	////////////////
	//Rule 1. Character Object Name Related
	//bgr	
	if (scene._background._file != "-1" && scene._background._file != "-2")
	{
		name = scene._background._name;
		spriteList.push_back(name);
	}

	//hero
	name = scene._hero._name;
	for (int i = 0; i < scene._hero._charac.size(); i++)
	{
		std::string hero_name_prefix = name + "_hero_" + std::to_string(i + 1);

		//Face
		if (scene._hero._charac[i]._face._file != "-1" && scene._hero._charac[i]._face._file != "-2")
		{
			std::string face = hero_name_prefix + "_face";
			spriteList.push_back(face);
		}

		//Hair
		if (scene._hero._charac[i]._hair[0]._file != "-1" && scene._hero._charac[i]._hair[0]._file != "-2")
		{
			std::string hair_1 = hero_name_prefix + "_hair_hair_1";
			spriteList.push_back(hair_1);
		}

		if (scene._hero._charac[i]._hair[1]._file != "-1" && scene._hero._charac[i]._hair[1]._file != "-2")
		{
			std::string hair_2 = hero_name_prefix + "_hair_hair_2";
			spriteList.push_back(hair_2);
		}

		//Body
		if (scene._hero._charac[i]._body._file != "-1" && scene._hero._charac[i]._body._file != "-2")
		{
			std::string body = hero_name_prefix + "_body";
			spriteList.push_back(body);
		}

		//arm_right
		if (scene._hero._charac[i]._arm_right._file != "-1" && scene._hero._charac[i]._arm_right._file != "-2")
		{
			std::string arm_right = hero_name_prefix + "_arm_right";
			spriteList.push_back(arm_right);
		}

		//arm_left
		if (scene._hero._charac[i]._arm_left._file != "-1" && scene._hero._charac[i]._arm_left._file != "-2")
		{
			std::string arm_left = hero_name_prefix + "_arm_left";
			spriteList.push_back(arm_left);
		}

		//object
		std::string object = hero_name_prefix + "_object";
		for (int j = 0; j < scene._hero._charac[i]._object.size(); j++)
		{
			if (scene._hero._charac[i]._object[j]._file != "-1" && scene._hero._charac[i]._object[j]._file != "-2")
			{
				std::string object_object = object + "_object_" + std::to_string(j + 1);
				spriteList.push_back(object_object);
			}
		}
	}

	//charac
	for (int i = 0; i < scene._charac.size(); i++)
	{
		name = scene._charac[i]._name;
		std::string charac_name_prefix = name;

		//Face
		std::string face = charac_name_prefix + "_face";
		if (scene._charac[i]._face._file != "-1" && scene._charac[i]._face._file != "-2")
		{
			spriteList.push_back(face);
		}

		//Hair
		std::string hair_1 = charac_name_prefix + "_hair_hair_1";
		if (scene._charac[i]._hair[0]._file != "-1" && scene._charac[i]._hair[0]._file != "-2")
		{
			spriteList.push_back(hair_1);
		}

		std::string hair_2 = charac_name_prefix + "_hair_hair_2";
		if (scene._charac[i]._hair[1]._file != "-1" && scene._charac[i]._hair[1]._file != "-2")
		{
			spriteList.push_back(hair_2);
		}

		//Body
		std::string body = charac_name_prefix + "_body";
		if (scene._charac[i]._body._file != "-1" && scene._charac[i]._body._file != "-2")
		{
			spriteList.push_back(body);
		}

		//arm_right
		std::string arm_right = charac_name_prefix + "_arm_right";
		if (scene._charac[i]._arm_right._file != "-1" && scene._charac[i]._arm_right._file != "-2")
		{
			spriteList.push_back(arm_right);
		}

		//arm_left
		std::string arm_left = charac_name_prefix + "_arm_left";
		if (scene._charac[i]._arm_left._file != "-1" && scene._charac[i]._arm_left._file != "-2")
		{
			spriteList.push_back(arm_left);
		}

		//object
		std::string object = charac_name_prefix + "_object";
		for (int j = 0; j < scene._charac[i]._object.size(); j++)
		{
			if (scene._charac[i]._object[j]._file != "-1" && scene._charac[i]._object[j]._file != "-2")
			{
				std::string object_object = object + "_object_" + std::to_string(j + 1);
				spriteList.push_back(object_object);
			}
		}
	}

	//object
	for (int i = 0; i < scene._object.size(); i++)
	{
		name = scene._object[i]._name;
		if (scene._object[i]._file != "-1" && scene._object[i]._file != "-2")
		{
			spriteList.push_back(name);
		}
	}

	//object
	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		name = scene._subtitleStack[i]._name;
		if (scene._subtitleStack[i]._file != "-1" && scene._subtitleStack[i]._file != "-2")
		{
			spriteList.push_back(name);
		}
	}

	for (int i = 0; i < spriteList.size(); i++)
	{
		bool definedOrNot = false;
		for (int j = 0; j < scene._posStack._sprites.size(); j++)
		{
			if (spriteList[i] == scene._posStack._sprites[j])
			{
				definedOrNot = true;
				break;
			}
		}
		if (definedOrNot == false)
		{
			std::string info = "Error : " + spriteList[i];
			info = info + " -> sprite is not defined on Position Stack";
			_related_PosStack_Info.push_back(info);
		}
	}

	for (int i = 0; i < scene._posStack._sprites.size(); i++)
	{
		bool definedOrNot = false;
		for (int j = 0; j < spriteList.size(); j++)
		{
			if (spriteList[j] == scene._posStack._sprites[i])
			{
				definedOrNot = true;
				break;
			}
		}
		if (definedOrNot == false)
		{
			std::string info = "Error : " + scene._posStack._sprites[i];
			info = info + " -> sprite on Position Stack is not defined";
			_related_PosStack_Info.push_back(info);
		}
	}

	if (scene._posStack._sprites.size() != spriteList.size())
	{
		std::string info = "Error : Defined sprite number on position stack and defined sprite object number are different " + std::to_string(scene._posStack._sprites.size()) + " <-> " + std::to_string(spriteList.size());
		_related_PosStack_Info.push_back(info);
	}

	if (scene._sprite_num != scene._posStack._sprites.size())
	{
		std::string info = "Error : Defined sprite object number on overall and defined sprite number on position stack are different " + std::to_string(scene._sprite_num) + " <-> " + std::to_string(scene._posStack._sprites.size());
		_related_PosStack_Info.push_back(info);
	}

	if (scene._sprite_num != spriteList.size())
	{
		std::string info = "Error : Defined sprite object number on overall and defined sprite object number are different " + std::to_string(scene._sprite_num) + " <-> " + std::to_string(spriteList.size());
		_related_PosStack_Info.push_back(info);
	}
}

void Validate_Anchor(Scene scene)
{
	std::string fileName;
	std::string name;
	std::string ext;

	////////////////
	//Rule 1. Character Object Name Related

	//hero
	name = scene._hero._name;
	for (int i = 0; i < scene._hero._charac.size(); i++)
	{
		std::string hero_name_prefix = name + "_hero_" + std::to_string(i + 1);

		//Face
		if (scene._hero._charac[i]._face._file != "-1" && scene._hero._charac[i]._face._file != "-2")
		{
			std::string anc = scene._hero._charac[i]._face._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._face._movement._anchor + " on Hero_" + std::to_string(i + 1) + " on face";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//Hair
		if (scene._hero._charac[i]._hair[0]._file != "-1" && scene._hero._charac[i]._hair[0]._file != "-2")
		{
			std::string anc = scene._hero._charac[i]._hair[0]._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._hair[0]._movement._anchor + " on Hero_" + std::to_string(i + 1) + " on hair[1]";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left)";
				_related_Anchor_Info.push_back(info);
			}
		}

		if (scene._hero._charac[i]._hair[1]._file != "-1" && scene._hero._charac[i]._hair[1]._file != "-2")
		{
			std::string anc = scene._hero._charac[i]._hair[1]._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._hair[1]._movement._anchor + " on Hero_" + std::to_string(i + 1) + " on hair[2]";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//Body
		if (scene._hero._charac[i]._body._file != "-1" && scene._hero._charac[i]._body._file != "-2")
		{
			std::string anc = scene._hero._charac[i]._body._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._body._movement._anchor + " on Hero_" + std::to_string(i + 1) + " on body";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//arm_right
		if (scene._hero._charac[i]._arm_right._file != "-1" && scene._hero._charac[i]._arm_right._file != "-2")
		{
			std::string anc = scene._hero._charac[i]._arm_right._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._arm_right._movement._anchor + " on Hero_" + std::to_string(i + 1) + " on arm_right";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//arm_left
		if (scene._hero._charac[i]._arm_left._file != "-1" && scene._hero._charac[i]._arm_left._file != "-2")
		{
			std::string anc = scene._hero._charac[i]._arm_left._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._arm_left._movement._anchor + " on  Hero_" + std::to_string(i + 1) + " on arm_left";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//object
		std::string object = hero_name_prefix + "_object";
		for (int j = 0; j < scene._hero._charac[i]._object.size(); j++)
		{
			if (scene._hero._charac[i]._object[j]._file != "-1" && scene._hero._charac[i]._object[j]._file != "-2")
			{
				std::string anc = scene._hero._charac[i]._object[j]._movement._anchor;
				if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
					anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
					|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
					anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
				{
					std::string info = "Error : " + scene._hero._charac[i]._object[j]._movement._anchor + " on Hero_" + std::to_string(i + 1) + " on Object_" + std::to_string(j + 1);
					info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left)";
					_related_Anchor_Info.push_back(info);
				}
			}
		}

		std::string anc = scene._hero._charac[i]._movement._anchor;
		if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
			anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
			|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
			anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
		{
			std::string info = "Error : " + scene._hero._charac[i]._movement._anchor + " on  Hero_" + std::to_string(i + 1);
			info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left)";
			_related_Anchor_Info.push_back(info);
		}
	}

	//charac
	for (int i = 0; i < scene._charac.size(); i++)
	{
		name = scene._charac[i]._name;
		std::string charac_name_prefix = name;

		//Face
		std::string face = charac_name_prefix + "_face";
		if (scene._charac[i]._face._file != "-1" && scene._charac[i]._face._file != "-2")
		{
			std::string anc = scene._charac[i]._face._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._charac[i]._face._movement._anchor + " on Charac_" + std::to_string(i + 1) + " on face";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//Hair
		std::string hair_1 = charac_name_prefix + "_hair_hair_1";
		if (scene._charac[i]._hair[0]._file != "-1" && scene._charac[i]._hair[0]._file != "-2")
		{
			std::string anc = scene._charac[i]._hair[0]._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._charac[i]._hair[0]._movement._anchor + " on Charac_" + std::to_string(i + 1) + " on hair[0]";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
				_related_Anchor_Info.push_back(info);
			}
		}

		std::string hair_2 = charac_name_prefix + "_hair_hair_2";
		if (scene._charac[i]._hair[1]._file != "-1" && scene._charac[i]._hair[1]._file != "-2")
		{
			std::string anc = scene._charac[i]._hair[1]._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._charac[i]._hair[1]._movement._anchor + " on Charac_" + std::to_string(i + 1) + " on hair[2]";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//Body
		std::string body = charac_name_prefix + "_body";
		if (scene._charac[i]._body._file != "-1" && scene._charac[i]._body._file != "-2")
		{
			std::string anc = scene._charac[i]._body._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._charac[i]._body._movement._anchor + " on Charac_" + std::to_string(i + 1) + " on body";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//arm_right
		std::string arm_right = charac_name_prefix + "_arm_right";
		if (scene._charac[i]._arm_right._file != "-1" && scene._charac[i]._arm_right._file != "-2")
		{
			std::string anc = scene._charac[i]._arm_right._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._charac[i]._arm_right._movement._anchor + " on Charac_" + std::to_string(i + 1) + " on arm_right";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//arm_left
		std::string arm_left = charac_name_prefix + "_arm_left";
		if (scene._charac[i]._arm_left._file != "-1" && scene._charac[i]._arm_left._file != "-2")
		{
			std::string anc = scene._charac[i]._arm_left._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._charac[i]._arm_left._movement._anchor + " on  Charac_" + std::to_string(i + 1) + " on arm_left";
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
				_related_Anchor_Info.push_back(info);
			}
		}

		//object
		std::string object = charac_name_prefix + "_object";
		for (int j = 0; j < scene._charac[i]._object.size(); j++)
		{
			if (scene._charac[i]._object[j]._file != "-1" && scene._charac[i]._object[j]._file != "-2")
			{
				std::string anc = scene._charac[i]._object[j]._movement._anchor;
				if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
					anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
					|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
					anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
				{
					std::string info = "Error : " + scene._charac[i]._object[j]._movement._anchor + " on Object_" + std::to_string(i + 1) + " on Object_" + std::to_string(j + 1);
					info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
					_related_Anchor_Info.push_back(info);
				}
			}
		}

		std::string anc = scene._charac[i]._movement._anchor;
		if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
			anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
			|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
			anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
		{
			std::string info = "Error : " + scene._charac[i]._movement._anchor + " on  Charac_" + std::to_string(i + 1);
			info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
			_related_Anchor_Info.push_back(info);
		}
	}

	//object
	for (int i = 0; i < scene._object.size(); i++)
	{
		if (scene._object[i]._file != "-1" && scene._object[i]._file != "-2")
		{
			std::string anc = scene._object[i]._movement._anchor;
			if (!(anc == "-2" || anc == "left_top" || anc == "center_top" || anc == "right_top" || anc == "center_right" || anc == "right_bottom" ||
				anc == "center_bottom" || anc == "left_bottom" || anc == "center_left" || anc == "center"
				|| anc == "mid_left_top" || anc == "mid_right_top" || anc == "left_mid_top" || anc == "right_mid_top" ||
				anc == "left_mid_bottom" || anc == "right_mid_bottom" || anc == "mid_left_bottom" || anc == "mid_right_bottom"))
			{
				std::string info = "Error : " + scene._object[i]._movement._anchor + " on Object_" + std::to_string(i + 1);
				info = info + " -> Anchor Option should be one of (center, left_top, center_top, right_top, center_right, right_bottom, center_bottom, left_bottom, center_left, mid_left_top, mid_right_top, left_mid_top, right_mid_top, left_mid_bottom, right_mid_bottom, mid_left_bottom, mid_right_bottom)";
				_related_Anchor_Info.push_back(info);
			}
		}
	}
}




void Validate_Animation(Scene scene)
{
	std::string fileName;
	std::string name;
	std::string ext;

	////////////////
	//Rule 1. Character Object Name Related
	//hero
	std::vector<bool> animationObjectExistence(scene._animationStack._animations.size());
	for (int i = 0; i < scene._animationStack._animations.size(); i++)
	{
		animationObjectExistence[i] = false;

		//bgr
		if (scene._animationStack._animations[i]._object == scene._background._name)
		{
			animationObjectExistence[i] = true;
		}
		if (animationObjectExistence[i] == true)	continue;

		//subtitle
		for (int j = 0; j < scene._subtitleStack.size(); j++)
		{
			if (scene._animationStack._animations[i]._object == scene._subtitleStack[j]._name)
			{
				animationObjectExistence[i] = true;
				break;
			}
		}
		if (animationObjectExistence[i] == true)	continue;

		//hero
		for (int j = 0; j < scene._hero._charac.size(); j++)
		{
			std::string currSpriteName = scene._hero._name + "_hero_" + std::to_string(j + 1);

			if (scene._animationStack._animations[i]._object == currSpriteName)
			{
				animationObjectExistence[i] = true;
				break;
			}
			if (animationObjectExistence[i] == true)	break;


			/////////////////////////////////////////////
			//Part
			//face
			if (scene._hero._charac[j]._face._file != "-1" && scene._hero._charac[j]._face._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_face";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//body
			if (scene._hero._charac[j]._body._file != "-1" && scene._hero._charac[j]._body._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_body";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//hair_1
			if (scene._hero._charac[j]._hair[0]._file != "-1" && scene._hero._charac[j]._hair[0]._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_hair_hair_1";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//hair_2
			if (scene._hero._charac[j]._hair[1]._file != "-1" && scene._hero._charac[j]._hair[1]._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_hair_hair_2";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//la
			if (scene._hero._charac[j]._arm_left._file != "-1" && scene._hero._charac[j]._arm_left._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_arm_left";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//ra
			if (scene._hero._charac[j]._arm_right._file != "-1" && scene._hero._charac[j]._arm_right._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_arm_right";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//object
			for (int k = 0; k < scene._hero._charac[j]._object.size(); k++)
			{
				if (scene._hero._charac[j]._object[k]._file != "-1" && scene._hero._charac[j]._object[k]._file != "-2")
				{
					std::string currSpriteName_part = currSpriteName + "_object_object_" + std::to_string(k + 1);
					if (scene._animationStack._animations[i]._object == currSpriteName_part)
					{
						animationObjectExistence[i] = true;
						break;
					}
				}
			}
			if (animationObjectExistence[i] == true)	break;
		}
		if (animationObjectExistence[i] == true)	continue;


		//charac
		for (int j = 0; j < scene._charac.size(); j++)
		{
			std::string currSpriteName = scene._charac[j]._name;

			if (scene._animationStack._animations[i]._object == currSpriteName)
			{
				animationObjectExistence[i] = true;
				break;
			}
			if (animationObjectExistence[i] == true)	break;

			/////////////////////////////////////////////
			//Part
			//face
			if (scene._charac[j]._face._file != "-1" && scene._charac[j]._face._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_face";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//body
			if (scene._charac[j]._body._file != "-1" && scene._charac[j]._body._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_body";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//hair_1
			if (scene._charac[j]._hair[0]._file != "-1" && scene._charac[j]._hair[0]._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_hair_hair_1";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//hair_2
			if (scene._charac[j]._hair[1]._file != "-1" && scene._charac[j]._hair[1]._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_hair_hair_2";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//la
			if (scene._charac[j]._arm_left._file != "-1" && scene._charac[j]._arm_left._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_arm_left";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//ra
			if (scene._charac[j]._arm_right._file != "-1" && scene._charac[j]._arm_right._file != "-2")
			{
				std::string currSpriteName_part = currSpriteName + "_arm_right";
				if (scene._animationStack._animations[i]._object == currSpriteName_part)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
			if (animationObjectExistence[i] == true)	break;

			//object
			for (int k = 0; k < scene._charac[j]._object.size(); k++)
			{
				if (scene._charac[j]._object[k]._file != "-1" && scene._charac[j]._object[k]._file != "-2")
				{
					std::string currSpriteName_part = currSpriteName + "_object_object_" + std::to_string(k + 1);
					if (scene._animationStack._animations[i]._object == currSpriteName_part)
					{
						animationObjectExistence[i] = true;
						break;
					}
				}
			}
			if (animationObjectExistence[i] == true)	break;
		}
		if (animationObjectExistence[i] == true)	continue;

		//object
		for (int k = 0; k < scene._object.size(); k++)
		{
			if (scene._object[k]._file != "-1" && scene._object[k]._file != "-2")
			{
				std::string currSpriteName = scene._object[k]._name;
				if (scene._animationStack._animations[i]._object == currSpriteName)
				{
					animationObjectExistence[i] = true;
					break;
				}
			}
		}
		if (animationObjectExistence[i] == true)	continue;

		//btn
		if (scene._animationStack._animations[i]._object == "btn_replay" ||
			scene._animationStack._animations[i]._object == "btn_home" ||
			scene._animationStack._animations[i]._object == "btn_previous" ||
			scene._animationStack._animations[i]._object == "btn_next" ||
			scene._animationStack._animations[i]._object == "btn_capture")
		{
			animationObjectExistence[i] = true;
		}
		if (animationObjectExistence[i] == true)	continue;

	}

	//Check!!!
	for (int i = 0; i < animationObjectExistence.size(); i++)
	{
		if (animationObjectExistence[i] == false)
		{
			std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._object;
			info = info + " -> " + "object name, " + scene._animationStack._animations[i]._object + " does not exist";
			_related_AniStack_Info.push_back(info);
		}
	}


	////////////////
	//Rule 2. Animation Name Related

	bool shouldBe_User_Emphasis_0 = true;
	bool shouldBe_User_Emphasis_1 = true;
	std::vector<int> user_animation_idx;
	std::vector<bool> user_animation_emphasis_idx_0;
	std::vector<bool> user_animation_emphasis_idx_1;


	for (int i = 0; i < scene._animationStack._animations.size(); i++)
	{
		std::string type = scene._animationStack._animations[i]._type;

		if (!(type == "enterance" || type == "exit" || type == "user" || type == "emphasis" || type == "btn_auto_touch"))
		{
			std::string info = "Error : " + scene._animationStack._animations[i]._type;
			info = info + " -> Animation type option should be one of (enterance, exit, user, emphasis)";
			_related_AniStack_Info.push_back(info);
		}

		if (type == "enterance")
		{
			std::string effect = scene._animationStack._animations[i]._effect;
			if (!(effect == "appear" || effect == "fade" || effect == "boom"))
			{
				std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> Animation effect option should be one of (appear, fade, boom)";
				_related_AniStack_Info.push_back(info);
			}
		}
		else if (type == "exit")
		{
			std::string effect = scene._animationStack._animations[i]._effect;
			if (!(effect == "appear" || effect == "fade"))
			{
				std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> Animation effect option should be one of (appear, fade)";
				_related_AniStack_Info.push_back(info);
			}
		}
		else if (type == "user")
		{
			user_animation_idx.push_back(i);
			std::string effect = scene._animationStack._animations[i]._effect;
			if (!(effect == "touch" || effect == "wait" || effect == "touch_or_wait" || effect == "drag_and_drop"))
			{
				std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> Animation effect option should be one of (touch, wait, touch_or_wait, drag_and_drop)";
				_related_AniStack_Info.push_back(info);
			}

			//emphasis 유무 확인 모듈
			for (int j = 0; j < scene._animationStack._animations[i]._option.size(); j++)
			{
				if (j == 0 || j == 1)
				{
					if ((isDefaultVal(scene._animationStack._animations[i]._option[j]) == true) || (scene._animationStack._animations[i]._option[j] != "true" && scene._animationStack._animations[i]._option[j] != "false"))
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect should have " + std::to_string(j + 1) + " - option (true or false)";
						_related_AniStack_Info.push_back(info);
					}
				}
				if (j == 0 && scene._animationStack._animations[i]._option[j] == "true")
				{
					shouldBe_User_Emphasis_0 = true;
				}
				else if (j == 0 && scene._animationStack._animations[i]._option[j] == "false")
				{
					shouldBe_User_Emphasis_0 = false;
				}
				else if (j == 1 && scene._animationStack._animations[i]._option[j] == "true")
				{
					shouldBe_User_Emphasis_1 = true;
				}
				else if (j == 1 && scene._animationStack._animations[i]._option[j] == "false")
				{
					shouldBe_User_Emphasis_1 = false;
				}
			}
			user_animation_emphasis_idx_0.push_back(shouldBe_User_Emphasis_0);
			user_animation_emphasis_idx_1.push_back(shouldBe_User_Emphasis_1);
		}
		else if (type == "emphasis")
		{
			std::string effect = scene._animationStack._animations[i]._effect;
			if (!(effect == "pulse" || effect == "borderline" || effect == "highlight" || effect == "aura" || effect == "size_bright_effect" || effect == "opacity" || effect == "exit" || effect == "boom" || effect == "button_cue"))
			{
				std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> Animation effect option should be one of (pulse, borderline, highlight, aura, size_bright_effect, exit, boom, button_cue)";
				_related_AniStack_Info.push_back(info);
			}

			if (effect == "pulse")
			{
				bool containAllValues = true;
				for (int j = 0; j < scene._animationStack._animations[i]._option.size(); j++)
				{
					if (isDefaultVal(scene._animationStack._animations[i]._option[j]) == true)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect should have " + std::to_string(j + 1) + " - option";
						_related_AniStack_Info.push_back(info);
					}

					if (j == 1)
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false && scene._animationStack._animations[i]._option[j] != "inf"&& scene._animationStack._animations[i]._option[j] != " inf")
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number or inf";
							_related_AniStack_Info.push_back(info);
						}
					}
					else
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
				}
			}

			else if (effect == "borderline")
			{
				bool containAllValues = true;
				for (int j = 0; j < 3; j++)
				{
					if (isDefaultVal(scene._animationStack._animations[i]._option[j]) == true)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect should have " + std::to_string(j + 1) + " - option";
						_related_AniStack_Info.push_back(info);
					}

					if (j == 0)
					{
						if (isVec3(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has color system (r, g, b)";
							_related_AniStack_Info.push_back(info);
						}
					}
					else if (j == 1)
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false && scene._animationStack._animations[i]._option[j] != "inf"&& scene._animationStack._animations[i]._option[j] != " inf")
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number or inf";
							_related_AniStack_Info.push_back(info);
						}
					}
					else {
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
				}
			}
			else if (effect == "highlight")
			{
				bool containAllValues = true;
				for (int j = 0; j < 3; j++)
				{
					if (isDefaultVal(scene._animationStack._animations[i]._option[j]) == true)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect should have " + std::to_string(j + 1) + " - option";
						_related_AniStack_Info.push_back(info);
					}

					if (j == 0)
					{
						if (isVec3(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
					else if (j == 1)
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false && scene._animationStack._animations[i]._option[j] != "inf"&& scene._animationStack._animations[i]._option[j] != " inf")
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number or inf";
							_related_AniStack_Info.push_back(info);
						}
					}
					else {
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
				}
			}
			else if (effect == "aura")
			{
				bool containAllValues = true;
				for (int j = 0; j < 4; j++)
				{
					if (isDefaultVal(scene._animationStack._animations[i]._option[j]) == true)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect should have " + std::to_string(j + 1) + " - option";
						_related_AniStack_Info.push_back(info);
					}

					if (j == 0)
					{
						if (isVec4(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
					else if (j == 3)
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false && scene._animationStack._animations[i]._option[j] != "inf" && scene._animationStack._animations[i]._option[j] != " inf")
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number or inf";
							_related_AniStack_Info.push_back(info);
						}
					}
					else {
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
				}
			}
			else if (effect == "size_bright_effect")
			{
				bool containAllValues = true;
				for (int j = 0; j < 3; j++)
				{
					if (isDefaultVal(scene._animationStack._animations[i]._option[j]) == true)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect should have " + std::to_string(j + 1) + " - option";
						_related_AniStack_Info.push_back(info);
					}

					if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
						_related_AniStack_Info.push_back(info);
					}
				}
			}
			else if (effect == "boom")
			{
				bool containAllValues = true;
				for (int j = 0; j < 3; j++)
				{
					if (isDefaultVal(scene._animationStack._animations[i]._option[j]) == true)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect should have " + std::to_string(j + 1) + " - option";
						_related_AniStack_Info.push_back(info);
					}

					if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
						_related_AniStack_Info.push_back(info);
					}
				}
			}
			else if (effect == "opacity")
			{
				bool containAllValues = true;
				for (int j = 0; j < 4; j++)
				{
					if (isDefaultVal(scene._animationStack._animations[i]._option[j]) == true)
					{
						std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
						info = info + " -> " + effect + " effect should have " + std::to_string(j + 1) + " - option";
						_related_AniStack_Info.push_back(info);
					}



					if (j == 0)
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
					else if (j == 1)
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
					else if (j == 2)
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false)
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number";
							_related_AniStack_Info.push_back(info);
						}
					}
					else if (j == 3)
					{
						if (isNumber(scene._animationStack._animations[i]._option[j]) == false && scene._animationStack._animations[i]._option[j] != "inf"&& scene._animationStack._animations[i]._option[j] != " inf")
						{
							std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
							info = info + " -> " + effect + " effect's " + std::to_string(j + 1) + " - option should has number or inf";
							_related_AniStack_Info.push_back(info);
						}
					}
				}
			}
		}
	}

	//User Interaction Test
	for (int i = 0; i < user_animation_idx.size(); i++)
	{
		int idx = user_animation_idx[i];

		if (user_animation_emphasis_idx_0[i] == true && user_animation_emphasis_idx_1[i] == true)
		{
			if (!(scene._animationStack._animations.size() > idx + 2))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that following-emphasis animation option is not defined";
				_related_AniStack_Info.push_back(info);
			}

			if (!(scene._animationStack._animations[idx]._object == scene._animationStack._animations[idx + 1]._object))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that following-emphasis animation option is not defined. Object name should be same";
				_related_AniStack_Info.push_back(info);
			}

			if (!(scene._animationStack._animations[idx]._object == scene._animationStack._animations[idx + 2]._object))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that following-emphasis animation option is not defined. Object name should be same";
				_related_AniStack_Info.push_back(info);
			}


			if (!(scene._animationStack._animations[idx + 1]._type == "emphasis"))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that there is no following-emphasis animation.";
				_related_AniStack_Info.push_back(info);
			}

			if (!(scene._animationStack._animations[idx + 2]._type == "emphasis"))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that there is no following-emphasis animation.";
				_related_AniStack_Info.push_back(info);
			}

			//Wait, Selection 유무
			if (!(scene._animationStack._animations[idx + 1]._action == "wait"))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that there is no following wait action for user action.";
				_related_AniStack_Info.push_back(info);
			}

			if (!(scene._animationStack._animations[idx + 2]._action == "selection"))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that there is no following selection action for user action";
				_related_AniStack_Info.push_back(info);
			}

		}

		if (user_animation_emphasis_idx_0[i] == true && user_animation_emphasis_idx_1[i] == false)
		{
			if (!(scene._animationStack._animations.size() > idx + 1))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that following-emphasis animation option is not defined";
				_related_AniStack_Info.push_back(info);
			}

			if (!(scene._animationStack._animations[idx]._object == scene._animationStack._animations[idx + 1]._object))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that following-emphasis animation option is not defined. Object name should be same";
				_related_AniStack_Info.push_back(info);
			}

			if (!(scene._animationStack._animations[idx + 1]._type == "emphasis"))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that there is no following-emphasis animation.";
				_related_AniStack_Info.push_back(info);
			}

			//Wait, Selection 유무
			if (!(scene._animationStack._animations[idx + 1]._action == "wait"))
			{
				std::string info = "Error : " + std::to_string(idx + 1) + " - line - animation, " + scene._animationStack._animations[i]._type;
				info = info + " -> It seems that there is no following wait action for user action.";
				_related_AniStack_Info.push_back(info);
			}
		}
	}


	//Time Value Test
	for (int i = 0; i < scene._animationStack._animations.size(); i++)
	{
		if (isNumber(scene._animationStack._animations[i]._time) != true)
		{
			std::string info = "Error : " + std::to_string(i + 1) + " - line - animation, time should be number!!";
			_related_AniStack_Info.push_back(info);
		}
	}
}


void Validate_Subtitle(Scene scene)
{
	std::string fileNameWOExt;
	std::string effectName;
	std::string ext;


	//file name
	if (scene._subtitle_num != scene._subtitleStack.size())
	{
		std::string info = "Error : subtitle number ";
		info = info + " -> It should have same size between subtitle num and subtitle stack size. " + std::to_string(scene._subtitle_num) + " <-> " + std::to_string(scene._subtitleStack.size());

		_related_Subtitle_Info.push_back(info);
	}



	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		fileNameWOExt = getFileNameWithOutExt(scene._subtitleStack[i]._file);

		if (scene._subtitleStack[i]._sound_file != "-1" && scene._subtitleStack[i]._sound_file != "-2")
		{
			string soundFileNameWOExt = getFileNameWithOutExt(scene._subtitleStack[i]._sound_file);

			if (fileNameWOExt != soundFileNameWOExt)
			{
				std::string info = "Error : " + scene._subtitleStack[i]._name + " - subtitle ";
				info = info + " -> It should have same name between subtitle text file name and subtitle sound file name. " + scene._subtitleStack[i]._file + " <-> " + scene._subtitleStack[i]._sound_file;

				_related_Subtitle_Info.push_back(info);
			}
		}
	}

	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		effectName = scene._subtitleStack[i]._effect;

		if (effectName != "none" && effectName != "shadow" && effectName != "outline" && effectName != "outline8")
		{
			std::string info = "Error : " + scene._subtitleStack[i]._name + " - subtitle ";
			info = info + " -> It should have one of effect among [none, shadow, outline, outline8]. Current value is " + effectName;
			_related_Subtitle_Info.push_back(info);
		}
	}

	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		string aligneName = scene._subtitleStack[i]._align;

		if (aligneName != "left" && aligneName != "right" && aligneName != "center")
		{
			std::string info = "Error : " + scene._subtitleStack[i]._name + " - subtitle ";
			info = info + " -> It should have one of align among [left, right, center]. Current value is " + aligneName;
			_related_Subtitle_Info.push_back(info);
		}
	}

	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		string fontEffectName = scene._subtitleStack[i]._font_effect;

		if (fontEffectName != "normal" && fontEffectName != "bold")
		{
			std::string info = "Error : " + scene._subtitleStack[i]._name + " - subtitle ";
			info = info + " -> It should have one of align among [normal, bold]. Current value is " + fontEffectName;
			_related_Subtitle_Info.push_back(info);
		}
	}

	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		if (isVec3(scene._subtitleStack[i]._color) != true && isVec4(scene._subtitleStack[i]._color) != true)
		{
			std::string info = "Error : " + scene._subtitleStack[i]._name + " - subtitle ";
			info = info + " -> It should have one of color system among [(R, G, B) or (R, G, B, A)]. Current value is " + scene._subtitleStack[i]._color;
			_related_Subtitle_Info.push_back(info);
		}
	}

	for (int i = 0; i < scene._subtitleStack.size(); i++)
	{
		if (isVec3(scene._subtitleStack[i]._effect_color) != true && isVec4(scene._subtitleStack[i]._effect_color) != true)
		{
			std::string info = "Error : " + scene._subtitleStack[i]._name + " - subtitle ";
			info = info + " -> It should have one of color system among [(R, G, B) or (R, G, B, A)]. Current value is " + scene._subtitleStack[i]._effect_color;
			_related_Subtitle_Info.push_back(info);
		}
	}
}






void Validate_Sound(Scene scene)
{
	//overall
	if (scene._sound._number != -2 && scene._sound._number != -1) {
		if (scene._sound._number != scene._sound._file.size())
		{
			std::string info = "Error : _sound._number";
			info = info + " -> sound number should be same or smaller than defined sound files. Current _sound._number : " + std::to_string(scene._sound._number);
			_related_Sound_Info.push_back(info);
		}
	}

	//hero
	string name = scene._hero._name;
	for (int i = 0; i < scene._hero._charac.size(); i++)
	{
		std::string hero_name_prefix = name + "_hero_" + std::to_string(i + 1);

		if (scene._hero._charac[i]._touchEvent._sound._number != -2 && scene._hero._charac[i]._touchEvent._sound._number != -1) {
			if (scene._hero._charac[i]._touchEvent._sound._number > scene._hero._charac[i]._touchEvent._sound._file.size())
			{
				std::string info = "Error : " + scene._hero._name + "_" + std::to_string(i + 1) + " touch effect sound";
				info = info + " -> sound number should be same or smaller than defined sound files";
				_related_Sound_Info.push_back(info);
			}
		}
	}

	//charac
	for (int i = 0; i < scene._charac.size(); i++)
	{
		name = scene._charac[i]._name;
		std::string charac_name_prefix = name;

		if (scene._charac[i]._touchEvent._sound._number != -2 && scene._charac[i]._touchEvent._sound._number != -1) {
			if (scene._charac[i]._touchEvent._sound._number > scene._charac[i]._touchEvent._sound._file.size())
			{
				std::string info = "Error : " + scene._charac[i]._name + "_" + std::to_string(i + 1) + " touch effect sound";
				info = info + " -> sound number should be same or smaller than defined sound files";
				_related_Sound_Info.push_back(info);
			}
		}
	}

	//object
	for (int i = 0; i < scene._object.size(); i++)
	{
		if (scene._object[i]._touchEvent._sound._number != -2 && scene._object[i]._touchEvent._sound._number != -1) {
			if (scene._object[i]._touchEvent._sound._number > scene._object[i]._touchEvent._sound._file.size())
			{
				std::string info = "Error : " + scene._object[i]._name + "_" + std::to_string(i + 1) + " touch effect sound";
				info = info + " -> sound number should be same or smaller than defined sound files";
				_related_Sound_Info.push_back(info);
			}
		}
	}
}



////////////////////////////////////////////////////////////////////////////////////////////////


void Validate_Rot(Scene scene)
{
	std::string fileName;
	std::string name;
	std::string ext;

	////////////////
	//Rule 1. Character Object Name Related

	//hero
	name = scene._hero._name;
	for (int i = 0; i < scene._hero._charac.size(); i++)
	{
		std::string hero_name_prefix = name + "_hero_" + std::to_string(i + 1);

		//Face
		if (scene._hero._charac[i]._face._file != "-1" && scene._hero._charac[i]._face._file != "-2")
		{
			std::string dic = scene._hero._charac[i]._face._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._face._movement._rot._direction + " on " + name + " Hero_" + std::to_string(i + 1) + " on face";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//Hair
		if (scene._hero._charac[i]._hair[0]._file != "-1" && scene._hero._charac[i]._hair[0]._file != "-2")
		{
			std::string dic = scene._hero._charac[i]._hair[0]._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._hair[0]._movement._rot._direction + " on " + name + " Hero_" + std::to_string(i + 1) + " on hair[1]";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		if (scene._hero._charac[i]._hair[1]._file != "-1" && scene._hero._charac[i]._hair[1]._file != "-2")
		{
			std::string dic = scene._hero._charac[i]._hair[1]._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._hair[1]._movement._rot._direction + " on " + name + " Hero_" + std::to_string(i + 1) + " on hair[2]";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//Body
		if (scene._hero._charac[i]._body._file != "-1" && scene._hero._charac[i]._body._file != "-2")
		{
			std::string dic = scene._hero._charac[i]._body._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._body._movement._rot._direction + " on " + name + " Hero_" + std::to_string(i + 1) + " on body";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//arm_right
		if (scene._hero._charac[i]._arm_right._file != "-1" && scene._hero._charac[i]._arm_right._file != "-2")
		{
			std::string dic = scene._hero._charac[i]._arm_right._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._arm_right._movement._rot._direction + " on " + name + " Hero_" + std::to_string(i + 1) + " on arm_right";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//arm_left
		if (scene._hero._charac[i]._arm_left._file != "-1" && scene._hero._charac[i]._arm_left._file != "-2")
		{
			std::string dic = scene._hero._charac[i]._arm_left._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._hero._charac[i]._arm_left._movement._rot._direction + " on " + name + " Hero_" + std::to_string(i + 1) + " on arm_left";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//object
		std::string object = hero_name_prefix + "_object";
		for (int j = 0; j < scene._hero._charac[i]._object.size(); j++)
		{
			if (scene._hero._charac[i]._object[j]._file != "-1" && scene._hero._charac[i]._object[j]._file != "-2")
			{
				std::string dic = scene._hero._charac[i]._object[j]._movement._rot._direction;
				if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
					dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
				{
					std::string info = "Error : " + scene._hero._charac[i]._object[j]._movement._rot._direction + " on " + name + " Hero_" + std::to_string(i + 1) + " on Object_" + std::to_string(j + 1);
					info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
					_related_Rot_Info.push_back(info);
				}
			}
		}

		std::string dic = scene._hero._charac[i]._movement._rot._direction;
		if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
			dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
		{
			std::string info = "Error : " + scene._hero._charac[i]._movement._rot._direction + " on " + name + " Hero_" + std::to_string(i + 1);
			info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
			_related_Rot_Info.push_back(info);
		}
	}

	//charac
	for (int i = 0; i < scene._charac.size(); i++)
	{
		name = scene._charac[i]._name;
		std::string charac_name_prefix = name;

		//Face
		std::string face = charac_name_prefix + "_face";
		if (scene._charac[i]._face._file != "-1" && scene._charac[i]._face._file != "-2")
		{
			std::string dic = scene._charac[i]._face._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._charac[i]._face._movement._rot._direction + " on Charac " + name + " on face";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//Hair
		std::string hair_1 = charac_name_prefix + "_hair_hair_1";
		if (scene._charac[i]._hair[0]._file != "-1" && scene._charac[i]._hair[0]._file != "-2")
		{
			std::string dic = scene._charac[i]._hair[0]._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._charac[i]._hair[0]._movement._rot._direction + " on Charac " + name + " on hair_1";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		std::string hair_2 = charac_name_prefix + "_hair_hair_2";
		if (scene._charac[i]._hair[1]._file != "-1" && scene._charac[i]._hair[1]._file != "-2")
		{
			std::string dic = scene._charac[i]._hair[1]._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._charac[i]._hair[1]._movement._rot._direction + " on Charac " + name + " on hair_2";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//Body
		std::string body = charac_name_prefix + "_body";
		if (scene._charac[i]._body._file != "-1" && scene._charac[i]._body._file != "-2")
		{
			std::string dic = scene._charac[i]._body._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._charac[i]._body._movement._rot._direction + " on Charac " + name + " on body";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//arm_right
		std::string arm_right = charac_name_prefix + "_arm_right";
		if (scene._charac[i]._arm_right._file != "-1" && scene._charac[i]._arm_right._file != "-2")
		{
			std::string dic = scene._charac[i]._arm_right._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._charac[i]._arm_right._movement._rot._direction + " on Charac " + name + " on arm_right";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//arm_left
		std::string arm_left = charac_name_prefix + "_arm_left";
		if (scene._charac[i]._arm_left._file != "-1" && scene._charac[i]._arm_left._file != "-2")
		{
			std::string dic = scene._charac[i]._arm_left._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._charac[i]._arm_left._movement._rot._direction + " on Charac " + name + " on arm_left";
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}

		//object
		std::string object = charac_name_prefix + "_object";
		for (int j = 0; j < scene._charac[i]._object.size(); j++)
		{
			if (scene._charac[i]._object[j]._file != "-1" && scene._charac[i]._object[j]._file != "-2")
			{
				std::string dic = scene._charac[i]._object[j]._movement._rot._direction;
				if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
					dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
				{
					std::string info = "Error : " + scene._charac[i]._object[j]._movement._rot._direction + " on Charac " + name + " on Object_" + std::to_string(j + 1);
					info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
					_related_Rot_Info.push_back(info);
				}
			}
		}

		std::string dic = scene._charac[i]._movement._rot._direction;
		if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
			dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
		{
			std::string info = "Error : " + scene._charac[i]._movement._rot._direction + " on  Charac " + name;
			info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
			_related_Rot_Info.push_back(info);
		}
	}

	//object
	for (int i = 0; i < scene._object.size(); i++)
	{
		if (scene._object[i]._file != "-1" && scene._object[i]._file != "-2")
		{
			std::string dic = scene._object[i]._movement._rot._direction;
			if (!(dic == "-2" || dic == "positive" || dic == "negative" || dic == "center_positive" || dic == "center_negative" || dic == "positive_oscillation" ||
				dic == "negative_oscillation" || dic == "center_positive_oscillation" || dic == "center_negative_oscillation"))
			{
				std::string info = "Error : " + scene._object[i]._movement._rot._direction + " on Object " + scene._object[i]._name;
				info = info + " -> rot/direction Option should be one of (positive, negative, center_positive, center_negative, positive_oscillation, negative_oscillation, center_positive_oscillation, center_negative_oscillation)";
				_related_Rot_Info.push_back(info);
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////

void ReadSubtitle(xml_node<>* node, int& subtitleNum, std::vector<Subtitle>& subtitleStack)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "subtitle_stack")
		{
			for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
			{
				std::string subsubCategoryName = pSubSubNode->name();

				if (subsubCategoryName == "content")
				{
					Subtitle subtitle;

					for (xml_node<> *pSubSubSubNode = pSubSubNode->first_node(); pSubSubSubNode; pSubSubSubNode = pSubSubSubNode->next_sibling())
					{
						std::string subsubsubCategoryName = pSubSubSubNode->name();
						std::string subsubsubCategoryValue = pSubSubSubNode->value();

						if (subsubsubCategoryName == "name")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._name = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._name = "-1";
							}
							else
							{
								subtitle._name = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "file")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._file = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._file = "-1";
							}
							else
							{
								subtitle._file = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "x")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._x = -2;
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._x = -1;
							}
							else
							{
								subtitle._x = std::stoi(subsubsubCategoryValue);
							}
						}
						else if (subsubsubCategoryName == "y")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._y = -2;
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._y = -1;
							}
							else
							{
								subtitle._y = std::stoi(subsubsubCategoryValue);
							}
						}
						else if (subsubsubCategoryName == "font")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._font = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._font = "-1";
							}
							else
							{
								subtitle._font = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "font_size")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._font_size = -2;
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._font_size = -1;
							}
							else
							{
								subtitle._font_size = std::stoi(subsubsubCategoryValue);
							}
						}
						else if (subsubsubCategoryName == "color")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._color = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._color = "-1";
							}
							else
							{
								subtitle._color = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "font_effect")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._font_effect = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._font_effect = "-1";
							}
							else
							{
								subtitle._font_effect = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "sound_file")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._sound_file = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._sound_file = "-1";
							}
							else
							{
								subtitle._sound_file = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "sound_volume")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._sound_volume = -2;
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._sound_volume = -1;
							}
							else
							{
								subtitle._sound_volume = std::stoi(subsubsubCategoryValue);
							}
						}
						else if (subsubsubCategoryName == "sound_option_1")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._sound_option_1 = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._sound_option_1 = "-1";
							}
							else
							{
								subtitle._sound_option_1 = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "sound_option_2")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._sound_option_2 = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._sound_option_2 = "-1";
							}
							else
							{
								subtitle._sound_option_2 = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "effect")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._effect = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._effect = "-1";
							}							
							else
							{
								subtitle._effect = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "effect_color")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._effect_color = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._effect_color = "-1";
							}
							else
							{
								subtitle._effect_color = subsubsubCategoryValue;
							}
						}
						else if (subsubsubCategoryName == "align")
						{
							if (isDefaultVal(subsubsubCategoryValue))
							{
								subtitle._align = "-2";
							}
							else if (isEmptyVal(subsubsubCategoryValue))
							{
								subtitle._align = "-1";
							}
							else
							{
								subtitle._align = subsubsubCategoryValue;
							}
						}
					}
					subtitleStack.push_back(subtitle);
				}
			}
		}
		else if (subCategoryName == "subtitle_num")
		{
			if (isDefaultVal(subCategoryValue))
			{
				subtitleNum = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				subtitleNum = -1;
			}
			else
			{
				subtitleNum = std::stoi(pSubNode->value());				
			}
		}
	}
}

void ReadBackground(xml_node<>* node, Background& background)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "name")
		{
			if (isDefaultVal(subCategoryValue))
			{
				background._name = "-2";
			}
			else if (isEmptyVal(subCategoryValue))
			{
				background._name = "-1";
			}
			else
			{
				background._name = subCategoryValue;
			}
		}
		else if (subCategoryName == "file")
		{
			if (isDefaultVal(subCategoryValue))
			{
				background._file = "-2";
			}
			else if (isEmptyVal(subCategoryValue))
			{
				background._file = "-1";
			}
			else
			{
				background._file = subCategoryValue;
			}
		}
		else if (subCategoryName == "width")
		{
			if (isDefaultVal(subCategoryValue))
			{
				background._width = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				background._width = -1;
			}
			else
			{
				background._width = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "height")
		{
			if (isDefaultVal(subCategoryValue))
			{
				background._height = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				background._height = -1;
			}
			else
			{
				background._height = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "view_port_init")
		{
			if (isDefaultVal(subCategoryValue))
			{
				Vec3_f c;
				c._x = -2;
				c._y = -2;
				c._z = -2;
				background._view_port_init = c;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				Vec3_f c;
				c._x = -1;
				c._y = -1;
				c._z = -1;
				background._view_port_init = c;
			}
			else
			{
				//	Check comma token
				int tokenCount = 0;
				for (int i = 0; i < subCategoryValue.size(); ++i)
					if (subCategoryValue[i] == ',') tokenCount++;

				if (tokenCount != 2) {
					std::string info = "Error : " + subCategoryName + " on Background";
					info = info + " -> Check the number of commas ','";
					_related_Background_Info.push_back(info);
				}

				Vec3_f c;
				ParsingToVec3_f(subCategoryValue, c);
				background._view_port_init = c;
			}
		}
		else if (subCategoryName == "view_port_final")
		{
			if (isDefaultVal(subCategoryValue))
			{
				Vec3_f c;
				c._x = -2;
				c._y = -2;
				c._z = -2;
				background._view_port_final = c;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				Vec3_f c;
				c._x = -1;
				c._y = -1;
				c._z = -1;
				background._view_port_final = c;
			}
			else
			{
				//	Check comma token
				int tokenCount = 0;
				for (int i = 0; i < subCategoryValue.size(); ++i)
					if (subCategoryValue[i] == ',') tokenCount++;

				if (tokenCount != 2) {
					std::string info = "Error : " + subCategoryName + " on Background";
					info = info + " -> Check the number of commas ','";
					_related_Background_Info.push_back(info);
				}

				Vec3_f c;
				ParsingToVec3_f(subCategoryValue, c);
				background._view_port_final = c;				
			}
		}
		else if (subCategoryName == "time")
		{
			if (isDefaultVal(subCategoryValue))
			{
				background._time = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				background._time = -1;
			}
			else
			{
				background._time = std::stoi(subCategoryValue);
			}
		}
	}
}

void ReadPositionStack(xml_node<>* node, PositionStack& posStack)
{
	for (xml_node<> *pSubSubNode = node->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
	{
		std::string subSubCategoryVal = pSubSubNode->value();
		posStack._sprites.push_back(subSubCategoryVal);
	}
}

void ReadAnimationStack(xml_node<>* node, AnimationStack& aniStack)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "animation")
		{
			Animation animation;
			for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
			{
				std::string subSubCategoryName = pSubSubNode->name();
				std::string subSubCategoryValue = pSubSubNode->value();

				if (subSubCategoryName == "object")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._object = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._object = "-1";
					}
					else
					{
						animation._object = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "type")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._type = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._type = "-1";
					}
					else
					{
						animation._type = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "effect")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._effect = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._effect = "-1";
					}
					else
					{
						animation._effect = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "time")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._time = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._time = "-1";
					}
					else
					{
						animation._time = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "action")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._action = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._action = "-1";
					}
					else
					{
						animation._action = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "option_1")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._option[0] = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._option[0] = "-1";
					}
					else
					{
						animation._option[0] = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "option_2")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._option[1] = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._option[1] = "-1";
					}
					else
					{
						animation._option[1] = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "option_3")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._option[2] = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._option[2] = "-1";
					}
					else
					{
						animation._option[2] = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "option_4")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._option[3] = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._option[3] = "-1";
					}
					else
					{
						animation._option[3] = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "option_5")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						animation._option[4] = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						animation._option[4] = "-1";
					}
					else
					{
						animation._option[4] = subSubCategoryValue;
					}
				}
			}
			aniStack._animations.push_back(animation);
		}
	}
}

void ReadSound(xml_node<>* node, Sound& sound)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "number")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sound._number = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sound._number = -1;
			}
			else
			{
				sound._number = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "contents")
		{
			for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
			{
				std::string subsubsubCategoryName = pSubSubNode->name();
				std::string subsubsubCategoryValue = pSubSubNode->value();

				if (subsubsubCategoryName == "file")
				{
					if (isDefaultVal(subsubsubCategoryValue))
					{
						sound._file.push_back("-2");
					}
					else if (isEmptyVal(subsubsubCategoryValue))
					{
						sound._file.push_back("-1");
					}
					else
					{
						sound._file.push_back(subsubsubCategoryValue);
					}
				}
				else if (subsubsubCategoryName == "volume")
				{
					if (isDefaultVal(subsubsubCategoryValue))
					{
						sound._volume.push_back(-2);
					}
					else if (isEmptyVal(subsubsubCategoryValue))
					{
						sound._volume.push_back(-1);
					}
					else
					{
						sound._volume.push_back(std::stoi(subsubsubCategoryValue));
					}
				}
			}
		}
	}
}

void ReadTransition(xml_node<>* node, std::vector<Transition>& transition)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "prev")
		{
			for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
			{
				std::string subsubCategoryName = pSubSubNode->name();
				std::string subsubCategoryValue = pSubSubNode->value();

				if (subsubCategoryName == "btn_show")
				{
					if (isDefaultVal(subsubCategoryValue))
					{
						transition[0]._btn_show = "-2";
					}
					else if (isEmptyVal(subsubCategoryValue))
					{
						transition[0]._btn_show = "-1";
					}
					else
					{
						transition[0]._btn_show = subsubCategoryValue;						
					}
				}
				else if (subsubCategoryName == "effect")
				{
					if (isDefaultVal(subsubCategoryValue))
					{
						transition[0]._effect = "-2";
					}
					else if (isEmptyVal(subsubCategoryValue))
					{
						transition[0]._effect = "-1";
					}
					else
					{
						transition[0]._effect = subsubCategoryValue;
					}
				}
				else if (subsubCategoryName == "direction")
				{
					if (isDefaultVal(subsubCategoryValue))
					{
						transition[0]._direction = "-2";
					}
					else if (isEmptyVal(subsubCategoryValue))
					{
						transition[0]._direction = "-1";
					}
					else
					{
						transition[0]._direction = subsubCategoryValue;
					}
				}
				else if (subsubCategoryName == "time")
				{
					if (isDefaultVal(subsubCategoryValue))
					{
						transition[0]._time = "-2";
					}
					else if (isEmptyVal(subsubCategoryValue))
					{
						transition[0]._time = "-1";
					}
					else
					{
						transition[0]._time = subsubCategoryValue;
					}
				}
			}
		}
		else if (subCategoryName == "next")
		{
			for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
			{
				std::string subsubCategoryName = pSubSubNode->name();
				std::string subsubCategoryValue = pSubSubNode->value();

				if (subsubCategoryName == "btn_show")
				{
					if (isDefaultVal(subsubCategoryValue))
					{
						transition[1]._btn_show = "-2";
					}
					else if (isEmptyVal(subsubCategoryValue))
					{
						transition[1]._btn_show = "-1";
					}
					else
					{
						transition[1]._btn_show = subsubCategoryValue;
					}
				}
				else if (subsubCategoryName == "effect")
				{
					if (isDefaultVal(subsubCategoryValue))
					{
						transition[1]._effect = "-2";
					}
					else if (isEmptyVal(subsubCategoryValue))
					{
						transition[1]._effect = "-1";
					}
					else
					{
						transition[1]._effect = subsubCategoryValue;
					}
				}
				else if (subsubCategoryName == "direction")
				{
					if (isDefaultVal(subsubCategoryValue))
					{
						transition[1]._direction = "-2";
					}
					else if (isEmptyVal(subsubCategoryValue))
					{
						transition[1]._direction = "-1";
					}
					else
					{
						transition[1]._direction = subsubCategoryValue;
					}
				}
				else if (subsubCategoryName == "time")
				{
					if (isDefaultVal(subsubCategoryValue))
					{
						transition[1]._time = "-2";
					}
					else if (isEmptyVal(subsubCategoryValue))
					{
						transition[1]._time = "-1";
					}
					else
					{
						transition[1]._time = subsubCategoryValue;
					}
				}
			}
		}
	}
}

void ReadHero(xml_node<>* node, Hero& hero)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		bool emptySpriteOrNot = true;
		if (subCategoryName == "name" && subCategoryValue == "-")  // 빈공간
		{
			emptySpriteOrNot = true;
			break;
		}
		else
		{
			std::string subCategoryName_sub = subCategoryName.substr(0, subCategoryName.rfind("_", subCategoryName.length()));

			if (subCategoryName == "name")
			{
				if (subCategoryValue.empty() == true)
				{
					hero._name = "-1";
				}
				else if (isDefaultVal(subCategoryValue))
				{
					hero._name = "-2";
				}
				else
				{
					hero._name = subCategoryValue;
					curr_group_name = subCategoryValue;
				}
			}
			else if (subCategoryName == "gender")
			{
				if (subCategoryValue.empty() == true)
				{
					hero._gender = "-1";
				}
				else if (isDefaultVal(subCategoryValue))
				{
					hero._gender = "-2";
				}
				else
				{
					hero._gender = subCategoryValue;
				}
			}
			else if (subCategoryName == "faceshift")
			{
				if (subCategoryValue.empty() == true)
				{
					hero._faceshift = "-1";
				}
				else if (isDefaultVal(subCategoryValue))
				{
					hero._faceshift = "-2";
				}
				else
				{
					if (subCategoryValue == "yes" || subCategoryValue == "true")
					{
						hero._faceshift = "true";
					}
					else if (subCategoryValue == "no" || subCategoryValue == "false")
					{
						hero._faceshift = "false";
					}
					else
					{
						std::string info = "Error : " + subCategoryName + " on Hero Section";
						info = info + " -> Option value should be 'yes' or 'no'";

						_related_Repeat_Info.push_back(info);
					}
				}
			}
			else if (subCategoryName == "number")
			{
				if (subCategoryValue.empty() == true)
				{
					hero._number = -1;
				}
				else if (isDefaultVal(subCategoryValue))
				{
					hero._number = -2;
				}
				else
				{
					hero._number = std::stoi(subCategoryValue);
				}
			}
			else if (subCategoryName_sub == "hero")
			{
				bool emptyHeroSpriteOrNot = true;
				Charac charac;
											
				for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
				{
					std::string subSubCategoryName = pSubSubNode->name();
					std::string subSubCategoryValue = pSubSubNode->value();
				
					if (subSubCategoryName == "face")
					{
						ReadFace(pSubSubNode, charac._face);
						IgnoreTouchEvent(charac._face._touchEvent);
					}
					else if (subSubCategoryName == "hair")
					{
						for (xml_node<> *pSubSubSubNode = pSubSubNode->first_node(); pSubSubSubNode; pSubSubSubNode = pSubSubSubNode->next_sibling())
						{
							std::string subSubCategoryName = pSubSubSubNode->name();
							std::string subSubCategoryValue = pSubSubSubNode->value();

							if (subSubCategoryName == "hair_1")
							{
								Sprite hair;
								ReadObject(pSubSubSubNode, hair);
								IgnoreTouchEvent(hair._touchEvent);
								charac._hair.push_back(hair);
							}
							else if (subSubCategoryName == "hair_2")
							{
								Sprite hair;
								ReadObject(pSubSubSubNode, hair);
								IgnoreTouchEvent(hair._touchEvent);
								charac._hair.push_back(hair);
							}
						}
					}
					else if (subSubCategoryName == "body")
					{
						ReadObject(pSubSubNode, charac._body);
						IgnoreTouchEvent(charac._body._touchEvent);
					}
					else if (subSubCategoryName == "arm_left")
					{
						ReadObject(pSubSubNode, charac._arm_left);
						IgnoreTouchEvent(charac._arm_left._touchEvent);
					}
					else if (subSubCategoryName == "arm_right")
					{
						ReadObject(pSubSubNode, charac._arm_right);
						IgnoreTouchEvent(charac._arm_right._touchEvent);
					}
					else if (subSubCategoryName == "object")
					{
						ReadObjects(pSubSubNode, charac._object);
						for (int i = 0; i < charac._object.size(); i++)
							IgnoreTouchEvent(charac._object[i]._touchEvent);
					}
					else if (subSubCategoryName == "movement")
					{
						ReadMovement(pSubSubNode, charac._movement);
					}
					else if (subSubCategoryName == "touch_event")
					{
						ReadTouchEvent(pSubSubNode, charac._touchEvent);
					}
				}

				//Check Empty Or Not based on File
				if (EmptyCharacOrNot(charac) == false) {
					hero._charac.push_back(charac);
				}
			}
		}

		//Charac sprite;
		//
		//for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
		//{
		//	std::string subSubCategoryName = pSubSubNode->name();
		//	std::string subSubCategoryValue = pSubSubNode->value();

		//	if (subSubCategoryName == "name" && subSubCategoryValue == "-")  // 빈공간
		//	{
		//		emptySpriteOrNot = true;
		//		break;
		//	}
		//	else
		//	{
		//		emptySpriteOrNot = false;

		//	}
		//}

		//if (emptySpriteOrNot == false)
		//{
		//	object.push_back(sprite);
		//}
	}
}

void ReadCharac(xml_node<>* node, std::vector<Charac>& charac)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{	
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		Charac sprite;
		bool emptySpriteOrNot = true;
		for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
		{
			std::string subSubCategoryName = pSubSubNode->name();
			std::string subSubCategoryValue = pSubSubNode->value();
			
			if (subSubCategoryName == "name" && subSubCategoryValue == "-")  // 빈공간
			{
				emptySpriteOrNot = true;
				break;
			}
			else
			{
				emptySpriteOrNot = false;

				if (subSubCategoryName == "name")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						sprite._name = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						sprite._name = "-1";
					}
					else
					{
						sprite._name = subSubCategoryValue;
						curr_group_name = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "gender")
				{
					if (isDefaultVal(subSubCategoryValue))
					{
						sprite._gender = "-2";
					}
					else if (isEmptyVal(subSubCategoryValue))
					{
						sprite._gender = "-1";
					}
					else
					{
						sprite._gender = subSubCategoryValue;
					}
				}
				else if (subSubCategoryName == "faceshift")
				{
					if (subCategoryValue.empty() == true)
					{
						sprite._faceshift = "-1";
					}
					else if (isDefaultVal(subCategoryValue))
					{
						sprite._faceshift = "-2";
					}
					else
					{
						if (subCategoryValue == "yes" || subCategoryValue == "true")
						{
							sprite._faceshift = "true";
						}
						else if (subCategoryValue == "no" || subCategoryValue == "false")
						{
							sprite._faceshift = "false";
						}
						else
						{
							std::string info = "Error : " + subSubCategoryName + " on " + subCategoryName;
							info = info + " -> Option value should be 'yes' or 'no'";

							_related_Repeat_Info.push_back(info);
						}
					}
				}
				else if (subSubCategoryName == "face")
				{
					ReadFace(pSubSubNode, sprite._face);
					IgnoreTouchEvent(sprite._face._touchEvent);
				}

				else if (subSubCategoryName == "hair")
				{
					for (xml_node<> *pSubSubSubNode = pSubSubNode->first_node(); pSubSubSubNode; pSubSubSubNode = pSubSubSubNode->next_sibling())
					{
						std::string subSubCategoryName = pSubSubSubNode->name();
						std::string subSubCategoryValue = pSubSubSubNode->value();

						if (subSubCategoryName == "hair_1")
						{
							Sprite hair;
							ReadObject(pSubSubSubNode, hair);
							IgnoreTouchEvent(hair._touchEvent);
							sprite._hair.push_back(hair);
						}
						else if (subSubCategoryName == "hair_2")
						{
							Sprite hair;
							ReadObject(pSubSubSubNode, hair);
							IgnoreTouchEvent(hair._touchEvent);
							sprite._hair.push_back(hair);
						}
					}
				}
				else if (subSubCategoryName == "body")
				{
					ReadObject(pSubSubNode, sprite._body);
					IgnoreTouchEvent(sprite._body._touchEvent);
				}
				else if (subSubCategoryName == "arm_left")
				{
					ReadObject(pSubSubNode, sprite._arm_left);
					IgnoreTouchEvent(sprite._arm_left._touchEvent);
				}
				else if (subSubCategoryName == "arm_right")
				{
					ReadObject(pSubSubNode, sprite._arm_right);
					IgnoreTouchEvent(sprite._arm_right._touchEvent);
				}
				else if (subSubCategoryName == "object")
				{
					ReadObjects(pSubSubNode, sprite._object);
					for (int i = 0; i < sprite._object.size(); i++) {
						IgnoreTouchEvent(sprite._object[i]._touchEvent);
					}
				}
				else if (subSubCategoryName == "movement")
				{
					ReadMovement(pSubSubNode, sprite._movement);
				}
				else if (subSubCategoryName == "touch_event")
				{
					ReadTouchEvent(pSubSubNode, sprite._touchEvent);
					//		sprite._height = std::stoi(subCategoryValue);
				}
			}
		}
		if (emptySpriteOrNot == false)
		{
			charac.push_back(sprite);
		}
	}
}

void ReadObject(xml_node<>* node, Sprite& sprite)
{
	bool emptySpriteOrNot = true;
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		//std::cout << "Object : " << subCategoryName << " : " << subCategoryValue << std::endl;

		if (subCategoryName == "file")
		{
			if (isDefaultVal(subCategoryValue))
				sprite._file = "-2";
			else if(isEmptyVal(subCategoryValue))
				sprite._file = "-1";
			else {
				sprite._file = subCategoryValue;
			}
		}
		else if (subCategoryName == "name")
		{
			if (isDefaultVal(subCategoryValue))
				sprite._name = "-2";
			else if (isEmptyVal(subCategoryValue))
				sprite._name = "-1";
			else {
				sprite._name = subCategoryValue;
				curr_object_name = subCategoryValue;
			}
		}
		else if (subCategoryName == "x")
		{
			if (isDefaultVal(subCategoryValue))
				sprite._x = -2;
			else if (isEmptyVal(subCategoryValue))
				sprite._x = -1;
			else {
				sprite._x = std::stoi(subCategoryValue);
			}

			//if (!isDefaultVal(subCategoryValue) && !isEmptyVal(subCategoryValue))
			//	sprite._x = std::stoi(subCategoryValue);
			//else
			//{
			//	sprite._x = -2;
			//}
		}
		else if (subCategoryName == "y")
		{
			if (isDefaultVal(subCategoryValue))
				sprite._y = -2;
			else if (isEmptyVal(subCategoryValue))
				sprite._y = -1;
			else {
				sprite._y = std::stoi(subCategoryValue);
			}

			//if (!isDefaultVal(subCategoryValue) && !isEmptyVal(subCategoryValue))
			//	sprite._y = std::stoi(subCategoryValue);
			//else
			//{
			//	sprite._y = -2;
			//}
		}
		else if (subCategoryName == "scale")
		{
			if (isDefaultVal(subCategoryValue))
				sprite._s = -2;
			else if (isEmptyVal(subCategoryValue))
				sprite._s = -1;
			else {
				sprite._s = std::stoi(subCategoryValue);
			}

			//if (!isDefaultVal(subCategoryValue) && !isEmptyVal(subCategoryValue))
			//	sprite._s = std::stof(subCategoryValue);
			//else
			//{
			//	sprite._s = -2;
			//}
		}
		else if (subCategoryName == "width")
		{
			if (isDefaultVal(subCategoryValue))
				sprite._width = -2;
			else if (isEmptyVal(subCategoryValue))
				sprite._width = -1;
			else {
				sprite._width = std::stoi(subCategoryValue);
			}

			//if (!isDefaultVal(subCategoryValue) && !isEmptyVal(subCategoryValue))
			//	sprite._width = std::stoi(subCategoryValue);
			//else
			//{
			//	sprite._width = -2;
			//}
		}
		else if (subCategoryName == "height")
		{
			if (isDefaultVal(subCategoryValue))
				sprite._height = -2;
			else if (isEmptyVal(subCategoryValue))
				sprite._height = -1;
			else {
				sprite._height = std::stoi(subCategoryValue);
			}

			//if (!isDefaultVal(subCategoryValue) && !isEmptyVal(subCategoryValue))
			//	sprite._height = std::stoi(subCategoryValue);
			//else
			//{
			//	sprite._height = -2;
			//}
		}
		else if (subCategoryName == "movement")
		{
			ReadMovement(pSubNode, sprite._movement);
		}
		else if (subCategoryName == "touch_event")
		{
			ReadTouchEvent(pSubNode, sprite._touchEvent);
		}
	}
}

bool ReadFace(xml_node<>* node, Face& sprite)
{
	bool emptySpriteOrNot = true;
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "file")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._file = "-2";
				sprite._x = -2;
				sprite._y = -2;
				sprite._s = -2;
				sprite._width = -2;
				sprite._height = -2;
				sprite._shape = -2;
				sprite._ang_x = -2;
				sprite._ang_y = -2;
				sprite._ang_z = -2;

				sprite._movement._anchor = -2;
				sprite._movement._effect._name = "-2";
				sprite._movement._effect._option_1 = -2;
				sprite._movement._effect._option_2 = -2;
				sprite._movement._effect._option_3 = -2;
				sprite._movement._effect._time = -2;

				sprite._movement._repeat._flag = -2;
				sprite._movement._repeat._time = -2;

				sprite._movement._rot._angle = -2;
				sprite._movement._rot._direction = -2;
				sprite._movement._rot._time = -2;

				sprite._movement._trans._s0 = -2;
				sprite._movement._trans._s1 = -2;
				sprite._movement._trans._time = -2;
				sprite._movement._trans._x0 = -2;
				sprite._movement._trans._x1 = -2;
				sprite._movement._trans._y0 = -2;
				sprite._movement._trans._y1 = -2;

				break;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._file = "-1";
				sprite._x = -1;
				sprite._y = -1;
				sprite._s = -1;
				sprite._width = -1;
				sprite._height = -1;
				sprite._shape = -1;
				sprite._ang_x = -1;
				sprite._ang_y = -1;
				sprite._ang_z = -1;

				sprite._movement._anchor = -1;
				sprite._movement._effect._name = "-1";
				sprite._movement._effect._option_1 = -1;
				sprite._movement._effect._option_2 = -1;
				sprite._movement._effect._option_3 = -1;
				sprite._movement._effect._time = -1;

				sprite._movement._repeat._flag = -1;
				sprite._movement._repeat._time = -1;

				sprite._movement._rot._angle = -1;
				sprite._movement._rot._direction = -1;
				sprite._movement._rot._time = -1;

				sprite._movement._trans._s0 = -1;
				sprite._movement._trans._s1 = -1;
				sprite._movement._trans._time = -1;
				sprite._movement._trans._x0 = -1;
				sprite._movement._trans._x1 = -1;
				sprite._movement._trans._y0 = -1;
				sprite._movement._trans._y1 = -1;
			}
			else
			{
				sprite._file = subCategoryValue;
			}
		}
		else if (subCategoryName == "x")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._x = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._x = -1;
			}
			else
			{
				sprite._x = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "y")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._y = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._y = -1;
			}
			else
			{
				sprite._y = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "scale")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._s = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._s = -1;
			}
			else
			{
				sprite._s = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "width")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._width = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._width = -1;
			}
			else
			{
				sprite._width = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "height")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._height = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._height = -1;
			}
			else
			{
				sprite._height = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "shape")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._shape = "-2";
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._shape = "-1";
			}
			else
			{
				sprite._shape = subCategoryValue;
			}
		}
		else if (subCategoryName == "ang_x")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._ang_x = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._ang_x = -1;
			}
			else
			{
				sprite._ang_x = std::stof(subCategoryValue);
			}
		}
		else if (subCategoryName == "ang_y")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._ang_y = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._ang_y = -1;
			}
			else
			{
				sprite._ang_y = std::stof(subCategoryValue);
			}	
		}
		else if (subCategoryName == "ang_z")
		{
			if (isDefaultVal(subCategoryValue))
			{
				sprite._ang_z = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				sprite._ang_z = -1;
			}
			else
			{
				sprite._ang_z = std::stof(subCategoryValue);
			}
		}
		else if (subCategoryName == "movement")
		{
			ReadMovement(pSubNode, sprite._movement);
		}
		//else if (subCategoryName == "touch_event")
		//{
		//	ReadTouchEvent(pSubNode, sprite._touchEvent);
		//}
		else if (subCategoryName == "expression")
		{
			ReadExpression(pSubNode, sprite._expression);
		}

	}
	return true;
}

void ReadObjects(xml_node<>* node, std::vector<Sprite>& object)
{
	curr_group_name = "objects";
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		Sprite sprite;
		bool emptySpriteOrNot = true;
		for (xml_node<> *pSubSubNode = pSubNode->first_node(); pSubSubNode; pSubSubNode = pSubSubNode->next_sibling())
		{
			std::string subCategoryName = pSubSubNode->name();
			std::string subCategoryValue = pSubSubNode->value();

			if (subCategoryName == "name" && subCategoryValue == "-")  // 빈공간
			{
				emptySpriteOrNot = true;
				break;
			}
			else
			{
				emptySpriteOrNot = false;

				if (subCategoryName == "name")
				{
					if (subCategoryValue.empty() == true)
					{
						sprite._name = "-1";
					}
					else if (isDefaultVal(subCategoryValue))
					{
						sprite._name = "-2";
					}
					else
					{
						sprite._name = subCategoryValue;
						curr_object_name = subCategoryValue;
					}
				}
				else if (subCategoryName == "file")
				{
					if (subCategoryValue.empty() == true)
					{
						sprite._file = "-1";
					}
					else if (isDefaultVal(subCategoryValue))
					{
						sprite._file = "-2";
					}
					else
					{
						sprite._file = subCategoryValue;
					}
				}
				else if (subCategoryName == "x")
				{
					if (subCategoryValue.empty() == true)
					{
						sprite._x = -1;
					}
					else if (isDefaultVal(subCategoryValue))
					{
						sprite._x = -2;
					}
					else
					{
						sprite._x = std::stoi(subCategoryValue);
					}
				}
				else if (subCategoryName == "y")
				{
					if (subCategoryValue.empty() == true)
					{
						sprite._y = -1;
					}
					else if (isDefaultVal(subCategoryValue))
					{
						sprite._y = -2;
					}
					else
					{
						sprite._y = std::stoi(subCategoryValue);
					}
				}
				else if (subCategoryName == "scale")
				{
					if (subCategoryValue.empty() == true)
					{
						sprite._s = -1;
					}
					else if (isDefaultVal(subCategoryValue))
					{
						sprite._s = -2;
					}
					else
					{
						sprite._s = std::stoi(subCategoryValue);
					}
				}
				else if (subCategoryName == "width")
				{
					if (subCategoryValue.empty() == true)
					{
						sprite._width = -1;
					}
					else if (isDefaultVal(subCategoryValue))
					{
						sprite._width = -2;
					}
					else
					{
						sprite._width = std::stoi(subCategoryValue);
					}
				}
				else if (subCategoryName == "height")
				{
					if (subCategoryValue.empty() == true)
					{
						sprite._height = -1;
					}
					else if (isDefaultVal(subCategoryValue))
					{
						sprite._height = -2;
					}
					else
					{
						sprite._height = std::stoi(subCategoryValue);
					}
				}
				else if (subCategoryName == "movement")
				{
					ReadMovement(pSubSubNode, sprite._movement);
					//		sprite._height = std::stoi(subCategoryValue);
				}
				else if (subCategoryName == "touch_event")
				{
					ReadTouchEvent(pSubSubNode, sprite._touchEvent);
					//		sprite._height = std::stoi(subCategoryValue);
				}
			}
		}

		if (emptySpriteOrNot == false)
		{
			object.push_back(sprite);
		}
	}
}

void ReadMovement(xml_node<>* node, Movement& movement)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();
		
		if (subCategoryName == "repeat")
		{
			ReadRepeat(pSubNode, movement._repeat);
		}
		else if (subCategoryName == "anchor")
		{
			if (isDefaultVal(subCategoryValue))
			{
				movement._anchor = "-2";
			}
			else if (isEmptyVal(subCategoryValue))
			{
				movement._anchor = "-1";
			}
			else
			{
				movement._anchor = subCategoryValue;
			}
		}
		else if (subCategoryName == "trans")
		{
			ReadTrans(pSubNode, movement._trans);
		}
		else if (subCategoryName == "rot")
		{
			ReadRot(pSubNode, movement._rot);
		}
		else if (subCategoryName == "effect")
		{
			ReadEffect(pSubNode, movement._effect);
		}
	}
}

void ReadRepeat(xml_node<>* node, Repeat& repeat)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "flag")
		{
			if (isDefaultVal(subCategoryValue))
			{
				repeat._flag = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				repeat._flag = -1;
			}
			else
			{
				if (subCategoryValue == "true")
				{
					repeat._flag = 1;
				}
				else if (subCategoryValue == "false")
				{
					repeat._flag = 0;
				}
				else
				{
					std::string info = "Error : " + subCategoryName;
					info = info + " -> Option value should be 'true' or 'false'";

					_related_Repeat_Info.push_back(info);
				}
			}
		}
		else if (subCategoryName == "time")
		{
			if (isFloat(subCategoryValue) == false)
			{
				std::string info = "Error : " + subCategoryName;
				info = info + " -> Time value should be a number";

				_related_Repeat_Info.push_back(info);
				repeat._time = -2;
			}
			else if (isDefaultVal(subCategoryValue))
			{
				repeat._time = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				repeat._time = -1;
			}
			else
			{
				repeat._time = std::stof(subCategoryValue);
			}
		}
	}
}

void ReadTrans(xml_node<>* node, Trans& trans)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "x0")
		{
			if (isDefaultVal(subCategoryValue))
			{
				trans._x0 = -2;
			}
			else if(isEmptyVal(subCategoryValue))
			{
				trans._x0 = -1;
			}
			else
			{
				trans._x0 = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "y0")
		{
			if (isDefaultVal(subCategoryValue))
			{
				trans._y0 = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				trans._y0 = -1;
			}
			else
			{
				trans._y0 = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "s0")
		{
			if (isDefaultVal(subCategoryValue))
			{
				trans._s0 = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				trans._s0 = -1;
			}
			else
			{
				trans._s0 = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "x1")
		{
			if (isDefaultVal(subCategoryValue))
			{
				trans._x1 = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				trans._x1 = -1;
			}
			else
			{
				trans._x1 = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "y1")
		{
			if (isDefaultVal(subCategoryValue))
			{
				trans._y1 = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				trans._y1 = -1;
			}
			else
			{
				trans._y1 = std::stoi(subCategoryValue);

			}
		}
		else if (subCategoryName == "s1")
		{
			if (isDefaultVal(subCategoryValue))
			{
				trans._s1 = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				trans._s1 = -1;
			}
			else
			{
				trans._s1 = std::stof(subCategoryValue);
			}
		}
		else if (subCategoryName == "time")
		{
			if (isDefaultVal(subCategoryValue))
			{
				trans._time = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				trans._time = -1;
			}
			else
			{

				trans._time = std::stof(subCategoryValue);
			}
		}
	}
}

void ReadRot(xml_node<>* node, Rot& rot)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "angle")
		{
			if (isDefaultVal(subCategoryValue))
			{
				rot._angle = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				rot._angle = -1;
			}
			else
			{
				rot._angle = std::stof(subCategoryValue);
			}
		}
		else if (subCategoryName == "direction")
		{
			if (isDefaultVal(subCategoryValue))
			{
				rot._direction = "-2";
			}
			else if (isEmptyVal(subCategoryValue))
			{
				rot._direction = "-1";
			}
			else
			{
				rot._direction = subCategoryValue;
			}
		}
		else if (subCategoryName == "time")
		{
			if (isDefaultVal(subCategoryValue))
			{
				rot._time = -2;
			}
			else if (isEmptyVal(subCategoryValue))
			{
				rot._time = -1;
			}
			else
			{
				rot._time = std::stof(subCategoryValue);
			}
		}
	}
}

void ReadEffect(xml_node<>* node, Effect& effect)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "name")
		{
			if (isDefaultVal(subCategoryValue)) {
				effect._name = "-2";
			}
			else if (isEmptyVal(subCategoryValue)) {
				effect._name = "-1";
			}
			else
			{
				effect._name = subCategoryValue;
			}
		}
		else if (subCategoryName == "option_1")
		{
			if (isDefaultVal(subCategoryValue)) {
				effect._option_1 = "-2";
			}
			else if (isEmptyVal(subCategoryValue)) {
				effect._option_1 = "-1";
			}
			else if (isNumber(subCategoryValue) == false && isDefaultVal(subCategoryValue) == false)
			{
				std::string info = "Error : " + curr_group_name + " - " + curr_object_name;
				info = info + " -> effect Option should be number";
				_related_Effect_Info.push_back(info);
			}
			else
			{
				effect._option_1 = subCategoryValue;
			}
		}
		else if (subCategoryName == "option_2")
		{
			if (isDefaultVal(subCategoryValue)) {
				effect._option_2 = "-2";
			}
			else if (isEmptyVal(subCategoryValue)) {
				effect._option_2 = "-1";
			}
			else if (isNumber(subCategoryValue) == false && isDefaultVal(subCategoryValue) == false)
			{
				std::string info = "Error : " + curr_group_name + " - " + curr_object_name;
				info = info + " -> effect Option should be number";
				_related_Effect_Info.push_back(info);
			}
			else
			{
				effect._option_2 = subCategoryValue;
			}
		}
		else if (subCategoryName == "option_3")
		{
			if (isDefaultVal(subCategoryValue)) {
				effect._option_3 = "-2";
			}
			else if (isEmptyVal(subCategoryValue)) {
				effect._option_3 = "-1";
			}
			else if (isNumber(subCategoryValue) == false && isDefaultVal(subCategoryValue) == false)
			{
				std::string info = "Error : " + curr_group_name + " - " + curr_object_name;
				info = info + " -> effect Option should be number";
				_related_Effect_Info.push_back(info);
			}
			else
			{
				effect._option_3 = subCategoryValue;
			}
		}
		else if (subCategoryName == "time")
		{
			if (isDefaultVal(subCategoryValue)) {
				effect._time = -2;
			}
			else if (isEmptyVal(subCategoryValue)) {
				effect._time = -1;
			}
			else
			{
				effect._time = std::stof(subCategoryValue);
			}
		}
	}
}

void ReadExpression(xml_node<>* node, Expression& expression)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "left_eye")
		{
			if (isDefaultVal(subCategoryValue)){
				expression._left_eye = -2;
			}
			else if (isEmptyVal(subCategoryValue)) {
				expression._left_eye = -1;
			}
			else {
				expression._left_eye = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "right_eye")
		{
			if (isDefaultVal(subCategoryValue)) {
				expression._right_eye = -2;
			}
			else if (isEmptyVal(subCategoryValue)) {
				expression._right_eye = -1;
			}
			else{
				expression._right_eye = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "left_eye_brow")
		{
			if (isDefaultVal(subCategoryValue)) {
				expression._left_eye_brow = -2;
			}
			else if (isEmptyVal(subCategoryValue)) {
				expression._left_eye_brow = -1;
			}
			else {
				expression._left_eye_brow = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "right_eye_brow")
		{
			if (isDefaultVal(subCategoryValue)) {
				expression._right_eye_brow = -2;
			}
			else if (isEmptyVal(subCategoryValue)) {
				expression._right_eye_brow = -1;
			}
			else {
				expression._right_eye_brow = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "mouse")
		{
			if (isDefaultVal(subCategoryValue)) {
				expression._mouse = -2;
			}
			else if (isEmptyVal(subCategoryValue)) {
				expression._mouse = -1;
			}
			else {
				expression._mouse = std::stoi(subCategoryValue);
			}
		}
		else if (subCategoryName == "emotion")
		{
			if (isDefaultVal(subCategoryValue)) {
				expression._emotion = -2;
			}
			else if (isEmptyVal(subCategoryValue)) {
				expression._emotion = -1;
			}
			else {
				expression._emotion = std::stoi(subCategoryValue);
			}
		}
	}
}

void ReadTouchEvent(xml_node<>* node, TouchEvent& touchEvent)
{
	for (xml_node<> *pSubNode = node->first_node(); pSubNode; pSubNode = pSubNode->next_sibling())
	{
		std::string subCategoryName = pSubNode->name();
		std::string subCategoryValue = pSubNode->value();

		if (subCategoryName == "sound")
		{
			ReadSound(pSubNode, touchEvent._sound);
		}
		else if (subCategoryName == "effect")
		{
			ReadEffect(pSubNode, touchEvent._effect);
		}
		else if (subCategoryName == "rot")
		{
			ReadRot(pSubNode, touchEvent._rot);
		}
	}
}

void Validate_Background_Value(Scene scene)
{
	//bgr
	if (scene._background._file == "-1")
	{
		std::string info = "Error : background file";
		info = info + " -> File name should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (scene._background._height == -1)
	{
		std::string info = "Error : background height";
		info = info + " -> background height should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (scene._background._width == -1)
	{
		std::string info = "Error : background width";
		info = info + " -> background width should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (scene._background._time == -1)
	{
		std::string info = "Error : background time";
		info = info + " -> background time should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (scene._background._view_port_init._x == -1 && scene._background._view_port_init._y == -1 && scene._background._view_port_init._z == -1)
	{
		std::string info = "Error : background view_port_init";
		info = info + " -> background view_port_init should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (scene._background._view_port_final._x == -1 && scene._background._view_port_final._y == -1 && scene._background._view_port_final._z == -1)
	{
		std::string info = "Error : background view_port_final";
		info = info + " -> background view_port_final should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	for (int i = 0; i < scene._sound._file.size(); i++)
	{
		if (scene._sound._file[i] == "-1")
		{
			std::string info = "Error : sound_" + std::to_string(i) + "_file";
			info = info + " -> it should be specified, at least '-'";

			_related_Value_Info.push_back(info);
		}
	}

	for (int i = 0; i < scene._sound._volume.size(); i++)
	{
		if (scene._sound._file[i] == "-1")
		{
			std::string info = "Error : sound_" + std::to_string(i) + "_volume";
			info = info + " -> it should be specified, at least '-'";

			_related_Value_Info.push_back(info);
		}
	}
}

void Validate_Hero_Value(Hero hero)
{
	if (hero._number == -1)
	{
		std::string info = "Error : hero number ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (hero._name == "-1")
	{
		std::string info = "Error : hero name ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (hero._gender == "-1")
	{
		std::string info = "Error : hero gender ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (hero._faceshift == "-1")
	{
		std::string info = "Error : hero faceshift ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	for (int i = 0; i < hero._charac.size(); i++)
	{
		std::string objectName;

		objectName = "hero_" + std::to_string(i) + "_face";
		Validate_Face_Value(hero._charac[i]._face, objectName);

		objectName = "hero_" + std::to_string(i) + "_hair_0";
		Validate_Sprite_Value(hero._charac[i]._hair[0], objectName);

		objectName = "hero_" + std::to_string(i) + "_hair_1";
		Validate_Sprite_Value(hero._charac[i]._hair[1], objectName);

		objectName = "hero_" + std::to_string(i) + "_arm_left";
		Validate_Sprite_Value(hero._charac[i]._arm_left, objectName);

		objectName = "hero_" + std::to_string(i) + "_arm_right";
		Validate_Sprite_Value(hero._charac[i]._arm_right, objectName);

		objectName = "hero_" + std::to_string(i) + "_body";
		Validate_Sprite_Value(hero._charac[i]._body, objectName);

		for (int j = 0; j < hero._charac[i]._object.size(); j++)
		{
			objectName = "hero_" + std::to_string(i) + "_object_object_" + std::to_string(j + 1);
			Validate_Sprite_Value(hero._charac[i]._object[j], objectName);
		}

		objectName = "hero_" + std::to_string(i);
		Validate_Movement_Value(hero._charac[i]._movement, objectName);
		Validate_TouchEvent_Value(hero._charac[i]._touchEvent, objectName);

	}
}


void Validate_Charac_Value(Charac charac, string characName)
{
	std::string objectName;

	objectName = characName + "_face";
	Validate_Face_Value(charac._face, objectName);

	objectName = characName + "_hair_0";
	Validate_Sprite_Value(charac._hair[0], objectName);

	objectName = characName + "_hair_1";
	Validate_Sprite_Value(charac._hair[1], objectName);

	objectName = characName + "_arm_left";
	Validate_Sprite_Value(charac._arm_left, objectName);

	objectName = characName + "_arm_right";
	Validate_Sprite_Value(charac._arm_right, objectName);

	objectName = characName + "_body";
	Validate_Sprite_Value(charac._body, objectName);

	for (int i = 0; i < charac._object.size(); i++)
	{
		objectName = characName + "_object_object_" + std::to_string(i + 1);
		Validate_Sprite_Value(charac._object[i], objectName);
	}

	Validate_Movement_Value(charac._movement, characName);
	Validate_TouchEvent_Value(charac._touchEvent, characName);
}


void Validate_Face_Value(Face face, string objectName)
{
	if (face._file == "-1")
	{
		std::string info = "Error : " + objectName + "._file ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (face._x == -1)
	{
		std::string info = "Error : " + objectName + "._x ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (face._y == -1)
	{
		std::string info = "Error : " + objectName + "._y ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (face._s == -1)
	{
		std::string info = "Error : " + objectName + "._s ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (face._width == -1)
	{
		std::string info = "Error : " + objectName + "._width ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (face._height == -1)
	{
		std::string info = "Error : " + objectName + "._height ";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	//if (face._expression._emotion == -1)
	//{
	//	std::string info = "Error : " + objectName + "._expression._emotion ";
	//	info = info + " -> It should be specified, at least '-'";

	//	_related_Value_Info.push_back(info);
	//}
	//if (face._expression._left_eye == -1)
	//{
	//	std::string info = "Error : " + objectName + "._expression._left_eye ";
	//	info = info + " -> It should be specified, at least '-'";

	//	_related_Value_Info.push_back(info);
	//}
	//if (face._expression._right_eye == -1)
	//{
	//	std::string info = "Error : " + objectName + "._expression._right_eye ";
	//	info = info + " -> It should be specified, at least '-'";

	//	_related_Value_Info.push_back(info);
	//}
	//if (face._expression._mouse == -1)
	//{
	//	std::string info = "Error : " + objectName + "._expression._mouse ";
	//	info = info + " -> It should be specified, at least '-'";

	//	_related_Value_Info.push_back(info);
	//}
	//if (face._expression._left_eye_brow == -1)
	//{
	//	std::string info = "Error : " + objectName + "._expression._left_eye_brow ";
	//	info = info + " -> It should be specified, at least '-'";

	//	_related_Value_Info.push_back(info);
	//}
	//if (face._expression._right_eye_brow == -1)
	//{
	//	std::string info = "Error : " + objectName + "._expression._right_eye_brow ";
	//	info = info + " -> It should be specified, at least '-'";

	//	_related_Value_Info.push_back(info);
	//}

	//if (face._shape == "-1")
	//{
	//	std::string info = "Error : " + objectName + "._expression._shape ";
	//	info = info + " -> It should be specified, at least '-'";

	//	_related_Value_Info.push_back(info);
	//}

	Validate_Movement_Value(face._movement, objectName);
	Validate_TouchEvent_Value(face._touchEvent, objectName);

}



void Validate_Sprite_Value(Sprite sprite, string objectName)
{
	if (sprite._x == -1)
	{
		std::string info = "Error : " + objectName + " x";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (sprite._y == -1)
	{
		std::string info = "Error : " + objectName + " y";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (sprite._s == -1)
	{
		std::string info = "Error : " + objectName + " s";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (sprite._width == -1)
	{
		std::string info = "Error : " + objectName + " width";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (sprite._height == -1)
	{
		std::string info = "Error : " + objectName + " height";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	Validate_Movement_Value(sprite._movement, objectName);
	Validate_TouchEvent_Value(sprite._touchEvent, objectName);

}



void Validate_Movement_Value(Movement movement, string objectName)
{
	if (movement._anchor == "-1")
	{
		std::string info = "Error : " + objectName + " _movement._anchor";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._effect._name == "-1")
	{
		std::string info = "Error : " + objectName + " _movement._effect._name";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._effect._option_1 == "-1")
	{
		std::string info = "Error : " + objectName + " _movement._effect._option_1";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._effect._option_2 == "-1")
	{
		std::string info = "Error : " + objectName + " _movement._effect._option_2";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._effect._option_3 == "-1")
	{
		std::string info = "Error : " + objectName + " _movement._effect._option_3";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._effect._time == -1)
	{
		std::string info = "Error : " + objectName + " _movement._effect._time";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}


	if (movement._repeat._flag == -1)
	{
		std::string info = "Error : " + objectName + " _movement._repeat._flag";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._repeat._time == -1)
	{
		std::string info = "Error : " + objectName + " _movement._repeat._time";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._rot._angle == -1)
	{
		std::string info = "Error : " + objectName + " _movement._rot._angle";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._rot._direction == "-1")
	{
		std::string info = "Error : " + objectName + " _movement._rot._direction";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._rot._time == -1)
	{
		std::string info = "Error : " + objectName + " _movement._rot._time";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._trans._s0 == -1)
	{
		std::string info = "Error : " + objectName + " _movement._trans._s0";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._trans._s1 == -1)
	{
		std::string info = "Error : " + objectName + " _movement._trans._s1";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._trans._time == -1)
	{
		std::string info = "Error : " + objectName + " _movement._trans._time";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._trans._x0 == -1)
	{
		std::string info = "Error : " + objectName + " _movement._trans._x0";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._trans._x1 == -1)
	{
		std::string info = "Error : " + objectName + " _movement._trans._x1";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._trans._y0 == -1)
	{
		std::string info = "Error : " + objectName + " _movement._trans._y0";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._trans._y1 == -1)
	{
		std::string info = "Error : " + objectName + " _movement._trans._y1";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (movement._trans._x0 != -1 && movement._trans._x0 != -2)
	{
		if (movement._trans._x1 == -2)
		{
			std::string info = "Error : " + objectName + " _movement._trans._x1";
			info = info + " -> It should be specified, if x0 is specified";

			_related_Value_Info.push_back(info);
		}

		if (movement._trans._y0 == -2)
		{
			std::string info = "Error : " + objectName + " _movement._trans._y0";
			info = info + " -> It should be specified, if x0 is specified";

			_related_Value_Info.push_back(info);
		}

		if (movement._trans._y1 == -2)
		{
			std::string info = "Error : " + objectName + " _movement._trans._y1";
			info = info + " -> It should be specified, if x0 is specified";

			_related_Value_Info.push_back(info);
		}

		if (movement._trans._time == -2)
		{
			std::string info = "Error : " + objectName + " _movement._trans._time";
			info = info + " -> It should be specified, if x0 is specified";

			_related_Value_Info.push_back(info);
		}

		if (isNumber(std::to_string(movement._trans._time)) == false)
		{
			std::string info = "Error : " + objectName + " _movement._trans._time";
			info = info + " -> It should be number";

			_related_Value_Info.push_back(info);
		}

		if (movement._trans._s0 == -2)
		{
			std::string info = "Error : " + objectName + " _movement._trans._s0";
			info = info + " -> It should be specified, if x0 is specified";

			_related_Value_Info.push_back(info);
		}

		if (movement._trans._s1 == -2)
		{
			std::string info = "Error : " + objectName + " _movement._trans._s1";
			info = info + " -> It should be specified, if x0 is specified";

			_related_Value_Info.push_back(info);
		}
	}
}


void Validate_TouchEvent_Value(TouchEvent touchEvent, string objectName)
{
	if (touchEvent._effect._name == "-1")
	{
		std::string info = "Error : " + objectName + " _touchEvent._effect._name";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (touchEvent._effect._time == -1)
	{
		std::string info = "Error : " + objectName + " _touchEvent._effect._time";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (touchEvent._effect._option_1 == "-1")
	{
		std::string info = "Error : " + objectName + " _touchEvent._effect._option_1";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (touchEvent._effect._option_2 == "-1")
	{
		std::string info = "Error : " + objectName + " _touchEvent._effect._option_2";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (touchEvent._effect._option_3 == "-1")
	{
		std::string info = "Error : " + objectName + " _touchEvent._effect._option_3";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	
	if (touchEvent._sound._number == -1)
	{
		std::string info = "Error : " + objectName + " _touchEvent._sound._number";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

}




void Validate_Subtitle_Value(Subtitle subtitle, string objectName)
{
	if (subtitle._x == -1)
	{
		std::string info = "Error : " + objectName + "_x";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
	if (subtitle._y == -1)
	{
		std::string info = "Error : " + objectName + "_y";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._align == "-1")
	{
		std::string info = "Error : " + objectName + "_align";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._color == "-1")
	{
		std::string info = "Error : " + objectName + "_color";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._effect == "-1")
	{
		std::string info = "Error : " + objectName + "_effect";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._effect_color == "-1")
	{
		std::string info = "Error : " + objectName + "_effect_color";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._file == "-1")
	{
		std::string info = "Error : " + objectName + "_file";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._font == "-1")
	{
		std::string info = "Error : " + objectName + "_font";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._font_effect == "-1")
	{
		std::string info = "Error : " + objectName + "_font_effect";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._font_size == -1)
	{
		std::string info = "Error : " + objectName + "_font_size";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._name == "-1")
	{
		std::string info = "Error : " + objectName + "_name";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._sound_file == "-1")
	{
		std::string info = "Error : " + objectName + "_sound_file";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._sound_option_1 == "-1")
	{
		std::string info = "Error : " + objectName + "_sound_option_1";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}

	if (subtitle._sound_option_2 == "-1")
	{
		std::string info = "Error : " + objectName + "_sound_option_2";
		info = info + " -> It should be specified, at least '-'";

		_related_Value_Info.push_back(info);
	}
}


bool EmptyCharacOrNot(Charac charac)
{
	if (charac._face._file != "-1" && charac._face._file != "-2")
	{
		return false;
	}

	if (charac._hair[0]._file != "-1" && charac._hair[0]._file != "-2")
	{
		return false;
	}

	if (charac._hair[1]._file != "-1" && charac._hair[1]._file != "-2")
	{
		return false;
	}

	if (charac._body._file != "-1" && charac._body._file != "-2")
	{
		return false;
	}

	if (charac._arm_left._file != "-1" && charac._arm_left._file != "-2")
	{
		return false;
	}

	if (charac._arm_right._file != "-1" && charac._arm_right._file != "-2")
	{
		return false;
	}

	for (int i = 0; i < charac._object.size(); i++)
	{
		if (charac._object[i]._file != "-1" && charac._object[i]._file != "-2")
		{
			return false;
		}
	}

	return true;
}


void IgnoreTouchEvent(TouchEvent& touch)
{
	touch._sound._file.clear();
	touch._sound._number = -2;
	touch._sound._volume.clear();

	touch._effect._name = "-2";
	touch._effect._option_1 = "-2";
	touch._effect._option_2 = "-2";
	touch._effect._option_3 = "-2";
	touch._effect._time = -2;

	touch._rot._angle = -2;
	touch._rot._direction = "-2";
	touch._rot._time = -2;
}



void Validate_SceneName(std::vector<std::string> scene)
{
	for (int i = 0; i < scene.size() - 1; i++)
	{
		if (scene[scene.size() - 1] == scene[i])
		{
			std::string info = "Error : Scene names are duplicated -> " + scene[scene.size() - 1] + " <-> " + scene[i];
			_related_SceneName_Info.push_back(info);
		}
	}
}